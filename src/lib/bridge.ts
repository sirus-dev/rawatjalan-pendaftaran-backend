import * as fetch from 'node-fetch';
import * as CryptoJS from 'crypto-js';
import config from '../config';
import { PesertaBPJS, SEPRequest, getLastID, SEPResponse } from '../api/models';
import { ERROR_MESSAGE, functionLogger, dummySEP } from './index';
import * as moment from 'moment';
import { dumyRujukan } from './dummy';
const { CONSID, CONSSECRET, BASEURL } = config;

export const nullifiedBridgeData = {
    metaData: {
        code: null,
        message: null
    },
    response: {
        peserta: {
            cob: {
               nmAsuransi: null,
               noAsuransi: null,
               tglTAT: null,
               tglTMT: null
            },
            hakKelas: {
               keterangan: null,
               kode: null
            },
            informasi: {
               dinsos: null,
               noSKTM: null,
               prolanisPRB: null
            },
            jenisPeserta: {
               keterangan: null,
               kode: null
            },
            mr: {
               noMR: null,
               noTelepon: null
            },
            nama: null,
            nik: null,
            noKartu: null,
            pisa: null,
            provUmum: {
               kdProvider: null,
               nmProvider: null
            },
            sex: null,
            statusPeserta: {
               keterangan: null,
               kode: null
            },
            tglCetakKartu: null,
            tglLahir: null,
            tglTAT: null,
            tglTMT: null,
            umur: {
               umurSaatPelayanan: null,
               umurSekarang: null
            }
        }
    }
};

export const nullifiedSEPData = {
    catatan: null,
    diagnosa: null,
    jnsPelayanan: null,
    kelasRawat: null,
    noSep: null,
    penjamin: null,
    peserta: {
       asuransi: null,
       hakKelas: null,
       jnsPeserta: null,
       kelamin: null,
       nama: null,
       noKartu: null,
       noMr: null,
       tglLahir: null
    },
    informasi: {
       Dinsos: null,
       prolanisPRB: null,
       noSKTM: null
    },
    poli: null,
    poliEksekutif: null,
    tglSep: null
};

/**
 * Used to set header for bridging request
 * @param consID Consumer ID, Provided by BPJS
 * @param consSecret Consumer Secret, Provided by BPJS
 */
export async function setHeaderSignature(consID: any, consSecret: any) {
    functionLogger.debug('==setHeaderSignature==');
    var timestamp = await Math.floor(Date.now() / 1000).toString();
    var data = consID + '&' + timestamp;
    var hmac = await CryptoJS.HmacSHA256(data, consSecret);
    var signature = await CryptoJS.enc.Base64.stringify(hmac);
    functionLogger.debug('HMAC Value: ' + hmac);
    functionLogger.debug('ConsID Value: ' + consID);
    functionLogger.debug('Signature Value: ' + signature);
    functionLogger.debug('TimeStamp Value: ' + timestamp);
    var result = {
        hmac: hmac,
        consid: consID,
        signature: signature,
        time: timestamp
    };
    return result;
  }

/**
 * Function for bridge patient data by KTP to BPJS API
 * @param noKTP KTP ID
 * @param time Bridge Time
 */
export async function bridgeKTP(noKTP: any, time: any) {
    functionLogger.debug('==bridgeKTP==');

    // return nullified data if noKTP is invalid (less or more than 16)
    if (noKTP.length !== 16) {
        functionLogger.debug(`KTP Length Invalid, only ${noKTP.length} while it should be 16`);
        functionLogger.info(ERROR_MESSAGE.NIK_INVALID);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.NIK_INVALID;
        return nullifiedBridgeData;
    }
    var header = await setHeaderSignature(CONSID, CONSSECRET);

    // fetch data from BPJS API
    functionLogger.debug(`Fetching "${BASEURL}/Peserta/nik/${noKTP}/tglSEP/${time}"`);
    let result = await fetch(BASEURL + '/Peserta/nik/' + noKTP + '/tglSEP/' + time, {
        mode: 'cors',
        method: 'get',
        headers: {
        'X-Cons-ID': header.consid,
        'X-Timestamp': header.time,
        'X-Signature': header.signature
        }
    }).then(async function(response: any) {
        let data = await response.json();
        functionLogger.debug(data);

        // if response if found, map the response data, save it on local DB and return it as result
        if ( data.response !== null ) {
            functionLogger.info('User Found');
            let id = parseInt(await getLastID(PesertaBPJS, 'id_bpjs_peserta'), 10) + 1;

            // save the response data on local DB
            PesertaBPJS.findOrCreate({
                where: { nik: noKTP },
                defaults: {
                    id_bpjs_peserta: id,
                    nik: data.response.peserta.nik,
                    noKartu: data.response.peserta.noKartu,
                    nama: data.response.peserta.nama,
                    pisa: data.response.peserta.pisa,
                    sex: data.response.peserta.sex,
                    cob_nmAsuransi: data.response.peserta.cob.nmAsuransi,
                    cob_noAsuransi: data.response.peserta.cob.noAsuransi,
                    cob_tglTAT: data.response.peserta.cob.tglTAT,
                    cob_tglTMT: data.response.peserta.cob.tglTMT,
                    hakKelas_keterangan: data.response.peserta.hakKelas.keterangan,
                    hakKelas_kode: data.response.peserta.hakKelas.kode,
                    informasi_dinsos: data.response.peserta.informasi.dinsos,
                    informasi_noSKTM: data.response.peserta.informasi.noSKTM,
                    informasi_prolanisPRB: data.response.peserta.informasi.prolanisPRB,
                    jenisPeserta_keterangan: data.response.peserta.jenisPeserta.keterangan,
                    jenisPeserta_kode: data.response.peserta.jenisPeserta.kode,
                    mr_noMR: data.response.peserta.mr.noMR,
                    mr_noTelepon: data.response.peserta.mr.noTelepon,
                    provUmum_kdProvider: data.response.peserta.provUmum.kdProvider,
                    provUmum_nmProvider: data.response.peserta.provUmum.nmProvider,
                    statusPeserta_keterangan: data.response.peserta.statusPeserta.keterangan,
                    statusPeserta_kode: data.response.peserta.statusPeserta.kode,
                    tglCetakKartu: data.response.peserta.tglCetakKartu,
                    tglLahir: data.response.peserta.tglLahir,
                    tglTAT: data.response.peserta.tglTAT,
                    tglTMT: data.response.peserta.tglTMT,
                    umur_umurSaatPelayanan: data.response.peserta.umur.umurSaatPelayanan,
                    umur_umurSekarang: data.response.peserta.umurSekarang
                }
            })
            .spread((user, created) => {
                functionLogger.info('New Database Created: ' + created);
            });

            // return the data as result
            functionLogger.trace(data);
            return data;
        } else {

            // patient data is not found, returning nullified data as result
            functionLogger.info(ERROR_MESSAGE.CARD_NOT_FOUND);
            nullifiedBridgeData.metaData.code = '201';
            nullifiedBridgeData.metaData.message = ERROR_MESSAGE.CARD_NOT_FOUND;
            return nullifiedBridgeData;
        }

    // Can't connect to BPJS API return error, proceeding to get the result from local DB
    }).catch(async (error) => {
        functionLogger.info(ERROR_MESSAGE.OFFLINE);
        return bridgeLocalKTP(noKTP);
    });

    // return the result
    functionLogger.trace(result);
    return result;
}

/**
 * Function for bridge patient data by KTP to BPJS API
 * @param noBPJS BPJS ID
 * @param time Bridge Time
 */
export async function bridgeBPJS(noBPJS: any, time: any) {
    functionLogger.debug('==bridgeBPJS==');

    // return nullified data if noBPJS is invalid (less or more than 13)
    if (noBPJS.length !== 13) {
        functionLogger.debug(`BPJS Length Invalid, only ${noBPJS.length} while it should be 13`);
        functionLogger.info(ERROR_MESSAGE.BPJS_INVALID);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.BPJS_INVALID;
        return nullifiedBridgeData;
    }
    var header = await setHeaderSignature(CONSID, CONSSECRET);

    // fetch data from BPJS API
    functionLogger.debug(`Fetching "${BASEURL}/Peserta/nik/${noBPJS}/tglSEP/${time}"`);
    let result = await fetch(BASEURL + '/Peserta/noKartu/' + noBPJS + '/tglSEP/' + time, {
        mode: 'cors',
        method: 'get',
        headers: {
        'X-Cons-ID': header.consid,
        'X-Timestamp': header.time,
        'X-Signature': header.signature
        }
    }).then(async function(response: any) {
        let data = await response.json();
        functionLogger.debug(data);

        // if response if found, map the response data, save it on local DB and return it as result
        if ( data.response !== null ) {
            functionLogger.info('User Found');
            let id = parseInt(await getLastID(PesertaBPJS, 'id_bpjs_peserta'), 10) + 1;

            // save the response data on local DB
            PesertaBPJS.findOrCreate({
                where: { noKartu: noBPJS },
                defaults: {
                    id_bpjs_peserta: id,
                    nik: data.response.peserta.nik,
                    noKartu: data.response.peserta.noKartu,
                    nama: data.response.peserta.nama,
                    pisa: data.response.peserta.pisa,
                    sex: data.response.peserta.sex,
                    cob_nmAsuransi: data.response.peserta.cob.nmAsuransi,
                    cob_noAsuransi: data.response.peserta.cob.noAsuransi,
                    cob_tglTAT: data.response.peserta.cob.tglTAT,
                    cob_tglTMT: data.response.peserta.cob.tglTMT,
                    hakKelas_keterangan: data.response.peserta.hakKelas.keterangan,
                    hakKelas_kode: data.response.peserta.hakKelas.kode,
                    informasi_dinsos: data.response.peserta.informasi.dinsos,
                    informasi_noSKTM: data.response.peserta.informasi.noSKTM,
                    informasi_prolanisPRB: data.response.peserta.informasi.prolanisPRB,
                    jenisPeserta_keterangan: data.response.peserta.jenisPeserta.keterangan,
                    jenisPeserta_kode: data.response.peserta.jenisPeserta.kode,
                    mr_noMR: data.response.peserta.mr.noMR,
                    mr_noTelepon: data.response.peserta.mr.noTelepon,
                    provUmum_kdProvider: data.response.peserta.provUmum.kdProvider,
                    provUmum_nmProvider: data.response.peserta.provUmum.nmProvider,
                    statusPeserta_keterangan: data.response.peserta.statusPeserta.keterangan,
                    statusPeserta_kode: data.response.peserta.statusPeserta.kode,
                    tglCetakKartu: data.response.peserta.tglCetakKartu,
                    tglLahir: data.response.peserta.tglLahir,
                    tglTAT: data.response.peserta.tglTAT,
                    tglTMT: data.response.peserta.tglTMT,
                    umur_umurSaatPelayanan: data.response.peserta.umur.umurSaatPelayanan,
                    umur_umurSekarang: data.response.peserta.umurSekarang
                }
            })
            .spread((user, created) => {
                functionLogger.info('New Database Created: ' + created);
            });

            // return the data as result
            return data;
        } else {

            // patient data is not found, returning nullified data as result
            nullifiedBridgeData.metaData.code = '201';
            nullifiedBridgeData.metaData.message = ERROR_MESSAGE.CARD_NOT_FOUND;
            return nullifiedBridgeData;
        }

    // Can't connect to BPJS API returns error, procedding to get the result from local DB
    }).catch(async (error) => {
        functionLogger.info(ERROR_MESSAGE.OFFLINE);
        return bridgeLocalKTP(noBPJS);
    });
    return result;
}

/**
 * Function for bridge patient data by KTP to Local Database
 * @param noKTP KTP ID
 */
export async function bridgeLocalKTP(noKTP: any) {
    functionLogger.debug('==bridgeLocalKTP==');

    // return nullified data if noKTP is invalid (less or more than 16)
    if (noKTP.length !== 16) {
        functionLogger.debug(`KTP Length Invalid, only ${noKTP.length} while it should be 16`);
        functionLogger.info(ERROR_MESSAGE.NIK_INVALID);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.NIK_INVALID;
        return nullifiedBridgeData;
    }
    let data = await PesertaBPJS.findOne({ where: {nik: noKTP} });

    // if patient data is not found, returning nullified data as result
    if ( !data ) {
        functionLogger.info(ERROR_MESSAGE.CARD_NOT_FOUND);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.CARD_NOT_FOUND;
        return nullifiedBridgeData;
    }

    // map founded patient data into result variable
    functionLogger.info('User Found on Local DB');
    let result = {
        metaData: {
            code: '200',
            message: 'Data Didapat dari DB Lokal'
        },
        response: {
            peserta: {
                cob: {
                   nmAsuransi: data.cob_nmAsuransi,
                   noAsuransi: data.cob_noAsuransi,
                   tglTAT: data.cob_tglTAT,
                   tglTMT: data.cob_tglTMT
                },
                hakKelas: {
                   keterangan: data.hakKelas_keterangan,
                   kode: data.hakKelas_kode
                },
                informasi: {
                   dinsos: data.informasi_dinsos,
                   noSKTM: data.informasi_noSKTM,
                   prolanisPRB: data.informasi_prolanisPRB
                },
                jenisPeserta: {
                   keterangan: data.informasi_keterangan,
                   kode: data.informasi_kode
                },
                mr: {
                   noMR: data.informasi_noMR,
                   noTelepon: data.informasi_noTelepon
                },
                nama: data.nama,
                nik: data.nik,
                noKartu: data.noKartu,
                pisa: data.pisa,
                provUmum: {
                   kdProvider: data.provUmum_kdProvider,
                   nmProvider: data.provUmum_nmProvider
                },
                sex: data.sex,
                statusPeserta: {
                   keterangan: data.statusPeserta_keterangan,
                   kode: data.statusPeserta_kode
                },
                tglCetakKartu: data.tglCetakKartu,
                tglLahir: data.tglLahir,
                tglTAT: data.tglTAT,
                tglTMT: data.tglTMT,
                umur: {
                   umurSaatPelayanan: data.umur_umurSaatPelayanan,
                   umurSekarang: data.umur_umurSekarang
                }
            }
        }
    };

    // return result
    return result;
}

/**
 * Function for bridge patient data by BPJS ID to Local Database
 * @param noBPJS BPJS ID
 */
export async function bridgeLocalBPJS(noBPJS: any) {
    functionLogger.debug('==bridgeLocalBPJS==');

    // return nullified data if noBPJS is invalid (less or more than 16)
    if (noBPJS.length !== 13) {
        functionLogger.debug(`BPJS Length Invalid, only ${noBPJS.length} while it should be 13`);
        functionLogger.info(ERROR_MESSAGE.BPJS_INVALID);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.BPJS_INVALID;
        return nullifiedBridgeData;
    }

    // if patient data is not found, returning nullified data as result
    let data = await PesertaBPJS.findOne({ where: {nik: noBPJS} });
    if ( !data ) {
        functionLogger.info(ERROR_MESSAGE.NOT_FOUND);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.CARD_NOT_FOUND;
        return nullifiedBridgeData;
    }

    // map founded patient data into result variable
    functionLogger.info('User Found on Local DB');
    let result = {
        metaData: {
            code: '200',
            message: 'Data Didapat dari DB Lokal'
        },
        response: {
            peserta: {
                cob: {
                   nmAsuransi: data.cob_nmAsuransi,
                   noAsuransi: data.cob_noAsuransi,
                   tglTAT: data.cob_tglTAT,
                   tglTMT: data.cob_tglTMT
                },
                hakKelas: {
                   keterangan: data.hakKelas_keterangan,
                   kode: data.hakKelas_kode
                },
                informasi: {
                   dinsos: data.informasi_dinsos,
                   noSKTM: data.informasi_noSKTM,
                   prolanisPRB: data.informasi_prolanisPRB
                },
                jenisPeserta: {
                   keterangan: data.informasi_keterangan,
                   kode: data.informasi_kode
                },
                mr: {
                   noMR: data.informasi_noMR,
                   noTelepon: data.informasi_noTelepon
                },
                nama: data.nama,
                nik: data.nik,
                noKartu: data.noKartu,
                pisa: data.pisa,
                provUmum: {
                   kdProvider: data.provUmum_kdProvider,
                   nmProvider: data.provUmum_nmProvider
                },
                sex: data.sex,
                statusPeserta: {
                   keterangan: data.statusPeserta_keterangan,
                   kode: data.statusPeserta_kode
                },
                tglCetakKartu: data.tglCetakKartu,
                tglLahir: data.tglLahir,
                tglTAT: data.tglTAT,
                tglTMT: data.tglTMT,
                umur: {
                   umurSaatPelayanan: data.umur_umurSaatPelayanan,
                   umurSekarang: data.umur_umurSekarang
                }
            }
        }
    };

    // return the result
    return result;
}

/**
 * Function to bridge rujukan to BPJS API
 * @param noBPJS BPJS ID
 */
export async function bridgeRujukan(noBPJS: any) {
    functionLogger.debug('==bridgeRujukan==');

    // return nullified data if noBPJS is invalid (less or more than 13)
    if (noBPJS.length !== 13) {
        functionLogger.debug(`BPJS Length Invalid, only ${noBPJS.length} while it should be 13`);
        functionLogger.info(ERROR_MESSAGE.BPJS_INVALID);
        nullifiedBridgeData.metaData.code = '201';
        nullifiedBridgeData.metaData.message = ERROR_MESSAGE.BPJS_INVALID;
        return nullifiedBridgeData;
    }
    var header = await setHeaderSignature(CONSID, CONSSECRET);

    // fetch data from BPSJ API
    functionLogger.debug(`Fetching "${BASEURL}/Rujukan/List/Peserta/${noBPJS}"`);
    // let result = await fetch(BASEURL + '/Rujukan/List/Peserta/' + noBPJS, {
    //     mode: 'cors',
    //     method: 'get',
    //     headers: {
    //     'X-Cons-ID': header.consid,
    //     'X-Timestamp': header.time,
    //     'X-Signature': header.signature
    //     }
    // }).then(async function(response: any) {
    //     const data = response.json();
    //     functionLogger.debug(data);
    //     return data;
    // }).catch(async (error) => {
    //     functionLogger.info(ERROR_MESSAGE.OFFLINE);
    // });
    let result = dumyRujukan;

    // return the result
    functionLogger.debug('Done Fetching');
    return result;
}

/**
 * Function to save request data, bridge to BPJS API, then save response data
 * @param args parsed from schema arguments
 */
export async function bridgeSEP(args: any) {
    functionLogger.debug('==bridgeSEP==');
    const sepID = parseInt(await getLastID(SEPRequest, 'id_bpjs_sep_request'), 10) + 1;
    functionLogger.debug('SEP ID: ' + sepID);

    // set the time to current time
    let tgl = moment().format('YYYY-MM-DD h:mm:ss');

    // save the request data into local DB
    await SEPRequest.create({
        id_bpjs_sep_request: sepID,
        noKartu: args.noKartu,
        tglSep: args.tglSep,
        ppkPelayanan: args.ppkPelayanan,
        jnsPelayanan: args.jnsPelayanan,
        klsRawat: args.klsRawat,
        noMR: args.noMR,
        rujukan_asalRujukan: args.rujukan_asalRujukan,
        rujukan_tglRujukan: args.rujukan_tglRujukan,
        rujukan_noRujukan: args.rujukan_noRujukan,
        rujukan_ppkRujukan: args.rujukan_ppkRujukan,
        catatan: args.catatan,
        diagAwal: args.diagAwal,
        poli_tujuan: args.poli_tujuan,
        poli_eksekutif: args.poli_eksekutif,
        cob_cob: args.cob,
        katarak_katarak: args.katarak,
        jmn_lakaLantas: args.jaminan_lakaLantas,
        jmn_pnjmn_penjamin: args.jaminan_penjamin,
        jmn_pnjmn_tglKejadian: args.jaminan_penjamin_tglKejadian,
        jmn_pnjmn_keterangan: args.jaminan_penjamin_keterangan,
        jmn_pnjmn_spls_suplesi: args.jaminan_penjamin_suplesi,
        jmn_pnjmn_spls_noSepSuplesi: args.jaminan_penjamin_suplesi_noSepSuplesi,
        jmn_pnjmn_spls_lksLaka_kdPropinsi: args.jaminan_penjamin_suplesi_lokasiLaka_kdPropinsi,
        jmn_pnjmn_spls_lksLaka_kdKabupaten: args.jaminan_penjamin_suplesi_lokasiLaka_kdKabupaten,
        jmn_pnjmn_spls_lksLaka_kdKecamatan: args.jaminan_penjamin_suplesi_lokasiLaka_kdKecamatan,
        skdp_noSurat: args.skdp_noSurat,
        skdp_kodeDPJP: args.skdp_kodeDPJP,
        noTelp: args.noTelp,
        user: args.user,
        createdAt: tgl
    });

    // map argument values into payload format for data fetching
    const payload = {
        request: {
            t_sep: {
                noKartu: args.noKartu,
                tglSep: args.tglSep,
                ppkPelayanan: args.ppkPelayanan,
                jnsPelayanan: args.jnsPelayanan,
                klsRawat: args.klsRawat,
                noMR: args.noMR,
                rujukan: {
                    asalRujukan: args.rujukan_asalRujukan,
                    tglRujukan: args.rujukan_tglRujukan,
                    noRujukan: args.rujukan_noRujukan,
                    ppkRujukan: args.rujukan_ppkRujukan
                },
                catatan: args.catatan,
                diagAwal: args.diagAwal,
                poli: {
                    tujuan: args.poli_tujuan,
                    eksekutif: args.poli_eksekutif
                },
                cob: {
                    cob: args.cob
                },
                katarak: {
                    katarak: args.katarak
                },
                jaminan: {
                    lakaLantas: args.jaminan_lakaLantas,
                    penjamin: {
                        penjamin: args.jaminan_penjamin,
                        tglKejadian: args.jaminan_penjamin_tglKejadian,
                        keterangan: args.jaminan_penjamin_keterangan,
                        suplesi: {
                            suplesi: args.jaminan_penjamin_suplesi,
                            noSepSuplesi: args.jaminan_penjamin_noSepSuplesi,
                            lokasiLaka: {
                                kdPropinsi: args.jaminan_penjamin_suplesi_lokasiLaka_kdPropinsi,
                                kdKabupaten: args.jaminan_penjamin_suplesi_lokasiLaka_kdKabupaten,
                                kdKecamatan: args.jaminan_penjamin_suplesi_lokasiLaka_kdKecamatan
                            }
                        }
                    }
                },
                skdp: {
                    noSurat: args.skdp_noSurat,
                    kodeDPJP: args.skdp_kodeDPJP
                },
                noTelp: args.noTelp,
                user: args.user
            }
        }
    };
    functionLogger.trace(payload);
    var header = await setHeaderSignature(CONSID, CONSSECRET);

    // fetch the data from BPJS API
    functionLogger.debug(`Fetching "${BASEURL}/SEP/1.1/insert"`);
    // let result = await fetch(BASEURL + '/SEP/1.1/insert', {
    //     mode: 'cors',
    //     method: 'post',
    //     headers: {
    //     'X-Cons-ID': header.consid,
    //     'X-Timestamp': header.time,
    //     'X-Signature': header.signature
    //     },
    //     body: JSON.stringify(payload)
    // }).then(async function(response: any) {
    //     const res = response.json();
    //     functionLogger.debug(res);
    //     return res;
    // }).catch(async (error) => {
    //     functionLogger.info(ERROR_MESSAGE.OFFLINE);
    // });
    let result = dummySEP;
    functionLogger.debug('Done Fetching');

    // save bridge response data into local DB
    await SEPResponse.create({
        id_bpjs_sep_response: (parseInt(await getLastID(SEPResponse, 'id_bpjs_sep_response'), 10) + 1),
        id_bpjs_sep_request: sepID,
        noSep: result.response.sep.noSep,
        catatan: result.response.sep.catatan,
        diagnosa: result.response.sep.diagnosa,
        jnsPelayanan: result.response.sep.jnsPelayanan,
        kelasRawat: result.response.sep.kelasRawat,
        penjamin: result.response.sep.penjamin,
        peserta_asuransi: result.response.sep.peserta.asuransi,
        peserta_hakKelas: result.response.sep.peserta.hakKelas,
        peserta_jnsPeserta: result.response.sep.peserta.jnsPeserta,
        peserta_kelamin: result.response.sep.peserta.kelamin,
        peserta_nama: result.response.sep.peserta.nama,
        peserta_noKartu: result.response.sep.peserta.noKartu,
        peserta_noMr: result.response.sep.peserta.noMr,
        peserta_tglLahir: result.response.sep.peserta.tglLahir,
        informasi_Dinsos: result.response.sep.informasi.Dinsos,
        informasi_prolanisPRB: result.response.sep.informasi.prolanisPRB,
        informasi_noSKTM: result.response.sep.informasi.noSKTM,
        poli: result.response.sep.poli,
        poliEksekutif: result.response.sep.poliEksekutif,
        tglSep: result.response.sep.tglSep,
        createdAt: tgl
    });

    // return data
    functionLogger.trace(result);
    return result;
}