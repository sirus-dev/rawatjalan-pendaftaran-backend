import { User } from '../api/models';
import * as jwt from 'jsonwebtoken';
import config from '../config';
const { SECRET } = config;

/**
 * function to create JWT Token
 * @param payload Payload for JWT
 */
function getToken(payload: any) {
    var token = jwt.sign(payload, SECRET, {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
    });
    return token;
}
export { getToken };