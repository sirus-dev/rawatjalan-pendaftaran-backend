import { DTPasien } from '../api/models';
import { ERROR_MESSAGE } from './error';
import { functionLogger } from './logger';

const nullifiedLocalData = {
    no_medrec: null,
    sts: null,
    nama: null,
    nama1: null,
    nama2: null,
    nama3: null,
    id: null,
    no_ktp: null,
    nama_panggil: null,
    kelamin: null,
    tempat: null,
    tgl_lahir: null,
    tgl: null,
    bulan: null,
    tahun: null,
    status: null,
    agama: null,
    pekerjaan: null,
    perusahaan: null,
    desa: null,
    jln1: null,
    no: null,
    rt: null,
    rw: null,
    kecamatan: null,
    kota: null,
    telp: null,
    hubungan: null,
    id2: null,
    no_ktp2: null,
    sts2: null,
    nama_penjamin: null,
    nama_panggil2: null,
    nama21: null,
    nama22: null,
    nama23: null,
    pekerjaan2: null,
    perusahaan2: null,
    jln2: null,
    desa2: null,
    no2: null,
    rt2: null,
    rw2: null,
    kecamatan2: null,
    kota2: null,
    telp2: null,
    userid: null,
    jam: null,
    xtgl: null,
    komplek1: null,
    komplek2: null,
    tempat2: null,
    tgl2: null,
    bln2: null,
    tahun2: null,
    flag_cetak: null,
    jnspt1: null,
    jnspt2: null
};

/**
 * Get patient data on local DB using patient's KTP ID
 * @param noKTP KTP ID
 */
export async function getLocalDataByNIK(noKTP: any) {
    functionLogger.debug('==getLocalDataByNik==');

    // return nullified data if noKTP is invalid (less or more than 16 digits)
    if (noKTP.length !== 16) {
        functionLogger.debug(`KTP Length Invalid, only ${noKTP.length} while it should be 16`);
        functionLogger.info(ERROR_MESSAGE.NIK_INVALID);
        const status = ERROR_MESSAGE.NIK_INVALID;
        return [nullifiedLocalData, status];
    }
    functionLogger.debug(`NIK: ${noKTP}`);
    let data = await DTPasien.findOne({ where: {no_ktp: noKTP} });

    // if no data found, return nullified data
    if ( !data ) {
        functionLogger.info(ERROR_MESSAGE.CARD_NOT_FOUND);
        const status = 'Data Tidak Ditemukan';
        return [nullifiedLocalData, status];
    }
    functionLogger.debug(data);
    functionLogger.info('Data Found');
    const status = 'Data Ditemukan';

    // data found, return data
    return [data.dataValues, status];
}

/**
 * Get patient data on local DB using patient's name
 * @param nama Patient's Name
 */
export async function getLocalDataByName(nama: any) {
    functionLogger.debug('==getLocalDataByName==');
    functionLogger.info(`Name: ${nama}`);
    let data = await DTPasien.findOne({ where: {nama: nama} });

    // if no data found return nullified data
    if ( !data ) {
        functionLogger.info(ERROR_MESSAGE.CARD_NOT_FOUND);
        const status = 'Data Tidak Ditemukan';
        return [nullifiedLocalData, status];
    }
    functionLogger.debug(data);
    functionLogger.info('Data Found');
    const status = 'Data Ditemukan';

    // data found, return data
    return [data.dataValues, status];
}