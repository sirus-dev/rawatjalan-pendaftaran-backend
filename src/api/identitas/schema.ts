// Define graphql schema type for Identitas Pasien Operations
export const pasienType = `
type Pasien {
    dataPasien: DTPasien
    dataBPJS: PesertaBPJS
    status: Status
}

type DTPasien {
    no_medrec: String
    sts: String
    nama: String
    nama1: String
    nama2: String
    nama3: String
    id: String
    no_ktp: String
    nama_panggil: String
    kelamin: String
    tempat: String
    tgl_lahir: String
    tgl: String
    bulan: String
    tahun: String
    status: String
    agama: String
    pekerjaan: String
    perusahaan: String
    desa: String
    jln1: String
    no: String
    rt: String
    rw: String
    kecamatan: String
    kota: String
    telp: String
    hubungan: String
    id2: String
    no_ktp2: String
    sts2: String
    nama_penjamin: String
    nama_panggil2: String
    nama21: String
    nama22: String
    nama23: String
    pekerjaan2: String
    perusahaan2: String
    jln2: String
    desa2: String
    no2: String
    rt2: String
    rw2: String
    kecamatan2: String
    kota2: String
    telp2: String
    userid: String
    jam: String
    xtgl: String
    komplek1: String
    komplek2: String
    tempat2: String
    tgl2: String
    bln2: String
    tahun2: String
    flag_cetak: String
    jnspt1: String
    jnspt2: String
}

type PesertaBPJS {
    nik: String
    noKartu: String
    nama: String
    pisa: String
    sex: String
    cob_nmAsuransi: String
    cob_noAsuransi: String
    cob_tglTAT: String
    cob_tglTMT: String
    hakKelas_keterangan: String
    hakKelas_kode: String
    informasi_dinsos: String
    informasi_noSKTM: String
    informasi_prolanisPRB: String
    jenisPeserta_keterangan: String
    jenisPeserta_kode: String
    mr_noMR: String
    mr_noTelepon: String
    provUmum_kdProvider: String
    provUmum_nmProvider: String
    statusPeserta_keterangan: String
    statusPeserta_kode: String
    tglCetakKartu: String
    tglLahir: String
    tglTAT: String
    tglTMT: String
    umur_umurSaatPelayanan: String
    umur_umurSekarang: String
}

type Status {
    data_bpjs: String
    data_lokal: String
}

type Query {
    searchByName(
        nama: String!
    ): Pasien
    searchByNIK(
        no_kartu: String!
    ): Pasien
    searchByBPJS(
        no_kartu: String!
    ): Pasien
}

type Mutation {
    addPasien(
        sts: String
        nama: String
        nama1: String
        nama2: String
        nama3: String
        id: String
        no_ktp: String
        nama_panggil: String
        kelamin: String
        tempat: String
        tgl_lahir: String
        tgl: String
        bulan: String
        tahun: String
        status: String
        agama: String
        pekerjaan: String
        perusahaan: String
        desa: String
        jln1: String
        no: String
        rt: String
        rw: String
        kecamatan: String
        kota: String
        telp: String
        hubungan: String
        id2: String
        no_ktp2: String
        sts2: String
        nama_penjamin: String
        nama_panggil2: String
        nama21: String
        nama22: String
        nama23: String
        pekerjaan2: String
        perusahaan2: String
        jln2: String
        desa2: String
        no2: String
        rt2: String
        rw2: String
        kecamatan2: String
        kota2: String
        telp2: String
        userid: String
        jam: String
        xtgl: String
        komplek1: String
        komplek2: String
        tempat2: String
        tgl2: String
        bln2: String
        tahun2: String
        flag_cetak: String
        jnspt1: String
        jnspt2: String
    ): DTPasien
    updatePasien(
        no_medrec: String!
        sts: String
        nama: String
        nama1: String
        nama2: String
        nama3: String
        id: String
        no_ktp: String
        nama_panggil: String
        kelamin: String
        tempat: String
        tgl_lahir: String
        tgl: String
        bulan: String
        tahun: String
        status: String
        agama: String
        pekerjaan: String
        perusahaan: String
        desa: String
        jln1: String
        no: String
        rt: String
        rw: String
        kecamatan: String
        kota: String
        telp: String
        hubungan: String
        id2: String
        no_ktp2: String
        sts2: String
        nama_penjamin: String
        nama_panggil2: String
        nama21: String
        nama22: String
        nama23: String
        pekerjaan2: String
        perusahaan2: String
        jln2: String
        desa2: String
        no2: String
        rt2: String
        rw2: String
        kecamatan2: String
        kota2: String
        telp2: String
        userid: String
        jam: String
        xtgl: String
        komplek1: String
        komplek2: String
        tempat2: String
        tgl2: String
        bln2: String
        tahun2: String
        flag_cetak: String
        jnspt1: String
        jnspt2: String
    ): DTPasien
}

schema{
    query: Query
    mutation: Mutation
}
`;
