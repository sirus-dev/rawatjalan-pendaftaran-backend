import { DTPasien } from '../models';
import * as moment from 'moment';
import { bridgeKTP, bridgeBPJS, getLocalDataByNIK, getLocalDataByName } from '../../lib';
export const pasienResolver = {

    Query: {
        async searchByName(_: any, args: any) {
            let dataPasien = await getLocalDataByName(args.nama);
            const time = await moment().format('YYYY-MM-DD');
            let dataBPJS = await bridgeKTP(dataPasien[0].no_ktp, time);
            var data = `{` +
                `"dataPasien": { ` +
                    `"no_medrec": "${dataPasien[0].no_medrec}", ` +
                    `"sts": "${dataPasien[0].sts}", ` +
                    `"nama": "${dataPasien[0].nama}", ` +
                    `"nama1": "${dataPasien[0].nama1}", ` +
                    `"nama2": "${dataPasien[0].nama2}", ` +
                    `"nama3": "${dataPasien[0].nama3}", ` +
                    `"id": "${dataPasien[0].id}", ` +
                    `"no_ktp": "${dataPasien[0].no_ktp}", ` +
                    `"nama_panggil": "${dataPasien[0].nama_panggil}", ` +
                    `"kelamin": "${dataPasien[0].kelamin}", ` +
                    `"tempat": "${dataPasien[0].tempat}", ` +
                    `"tgl_lahir": "${dataPasien[0].tgl_lahir}", ` +
                    `"tgl": "${dataPasien[0].tgl}", ` +
                    `"bulan": "${dataPasien[0].bulan}", ` +
                    `"tahun": "${dataPasien[0].tahun}", ` +
                    `"status": "${dataPasien[0].status}", ` +
                    `"agama": "${dataPasien[0].agama}", ` +
                    `"pekerjaan": "${dataPasien[0].pekerjaan}", ` +
                    `"perusahaan": "${dataPasien[0].perusahaan}", ` +
                    `"desa": "${dataPasien[0].desa}", ` +
                    `"jln1": "${dataPasien[0].jln1}", ` +
                    `"no": "${dataPasien[0].no}", ` +
                    `"rt": "${dataPasien[0].rt}", ` +
                    `"rw": "${dataPasien[0].rw}", ` +
                    `"kecamatan": "${dataPasien[0].kecamatan}", ` +
                    `"kota": "${dataPasien[0].kota}", ` +
                    `"telp": "${dataPasien[0].telp}", ` +
                    `"hubungan": "${dataPasien[0].hubungan}", ` +
                    `"id2": "${dataPasien[0].id2}", ` +
                    `"no_ktp2": "${dataPasien[0].no_ktp2}", ` +
                    `"sts2": "${dataPasien[0].sts2}", ` +
                    `"nama_penjamin": "${dataPasien[0].nama_penjamin}", ` +
                    `"nama_panggil2": "${dataPasien[0].nama_panggil2}", ` +
                    `"nama21": "${dataPasien[0].nama21}", ` +
                    `"nama22": "${dataPasien[0].nama22}", ` +
                    `"nama23": "${dataPasien[0].nama23}", ` +
                    `"pekerjaan2": "${dataPasien[0].pekerjaan2}", ` +
                    `"perusahaan2": "${dataPasien[0].perusahaan2}", ` +
                    `"jln2": "${dataPasien[0].jln2}", ` +
                    `"desa2": "${dataPasien[0].desa2}", ` +
                    `"no2": "${dataPasien[0].no2}", ` +
                    `"rt2": "${dataPasien[0].rt2}", ` +
                    `"rw2": "${dataPasien[0].rw2}", ` +
                    `"kecamatan2": "${dataPasien[0].kecamatan2}", ` +
                    `"kota2": "${dataPasien[0].kota2}", ` +
                    `"telp2": "${dataPasien[0].telp2}", ` +
                    `"userid": "${dataPasien[0].userid}", ` +
                    `"jam": "${dataPasien[0].jam}", ` +
                    `"xtgl": "${dataPasien[0].xtgl}", ` +
                    `"komplek1": "${dataPasien[0].komplek1}", ` +
                    `"komplek2": "${dataPasien[0].komplek2}", ` +
                    `"tempat2": "${dataPasien[0].tempat2}", ` +
                    `"tgl2": "${dataPasien[0].tgl2}", ` +
                    `"bln2": "${dataPasien[0].bln2}", ` +
                    `"tahun2": "${dataPasien[0].tahun2}", ` +
                    `"flag_cetak": "${dataPasien[0].flag_cetak}", ` +
                    `"jnspt1": "${dataPasien[0].jnspt1}", ` +
                    `"jnspt2": "${dataPasien[0].jnspt2}" ` +
                    `},` +
                `"dataBPJS": { ` +
                    `"nik": "${dataBPJS.response.peserta.nik}", ` +
                    `"noKartu": "${dataBPJS.response.peserta.noKartu}", ` +
                    `"nama": "${dataBPJS.response.peserta.nama}", ` +
                    `"pisa": "${dataBPJS.response.peserta.pisa}", ` +
                    `"sex": "${dataBPJS.response.peserta.sex}", ` +
                    `"cob_nmAsuransi": "${dataBPJS.response.peserta.cob.nmAsuransi}", ` +
                    `"cob_noAsuransi": "${dataBPJS.response.peserta.cob.noAsuransi}", ` +
                    `"cob_tglTAT": "${dataBPJS.response.peserta.cob.tglTAT}", ` +
                    `"cob_tglTMT": "${dataBPJS.response.peserta.cob.tglTMT}", ` +
                    `"hakKelas_keterangan": "${dataBPJS.response.peserta.hakKelas.keterangan}", ` +
                    `"hakKelas_kode": "${dataBPJS.response.peserta.hakKelas.kode}", ` +
                    `"informasi_dinsos": "${dataBPJS.response.peserta.informasi.dinsos}", ` +
                    `"informasi_noSKTM": "${dataBPJS.response.peserta.informasi.noSKTM}", ` +
                    `"informasi_prolanisPRB": "${dataBPJS.response.peserta.informasi.prolanisPRB}", ` +
                    `"jenisPeserta_keterangan": "${dataBPJS.response.peserta.jenisPeserta.keterangan}", ` +
                    `"jenisPeserta_kode": "${dataBPJS.response.peserta.jenisPeserta.kode}", ` +
                    `"mr_noMR": "${dataBPJS.response.peserta.mr.noMR}", ` +
                    `"mr_noTelepon": "${dataBPJS.response.peserta.mr.noTelepon}", ` +
                    `"provUmum_kdProvider": "${dataBPJS.response.peserta.provUmum.kdProvider}", ` +
                    `"provUmum_nmProvider": "${dataBPJS.response.peserta.provUmum.nmProvider}", ` +
                    `"statusPeserta_keterangan": "${dataBPJS.response.peserta.statusPeserta.keterangan}", ` +
                    `"statusPeserta_kode": "${dataBPJS.response.peserta.statusPeserta.kode}", ` +
                    `"tglCetakKartu": "${dataBPJS.response.peserta.tglCetakKartu}", ` +
                    `"tglLahir": "${dataBPJS.response.peserta.tglLahir}", ` +
                    `"tglTAT": "${dataBPJS.response.peserta.tglTAT}", ` +
                    `"tglTMT": "${dataBPJS.response.peserta.tglTMT}", ` +
                    `"umur_umurSaatPelayanan": "${dataBPJS.response.peserta.umur.umurSaatPelayanan}", ` +
                    `"umur_umurSekarang": "${dataBPJS.response.peserta.umur.umurSekarang}" ` +
                    `},` +
                `"status": {` +
                    `"data_bpjs": "${dataBPJS.metaData.message}", ` +
                    `"data_lokal": "${dataPasien[1]}" ` +
                    `}` +
                `}`;
            let result = JSON.parse(data);
            return result;
        },
        async searchByNIK(_: any, args: any) {
            const time = await moment().format('YYYY-MM-DD');
            let dataBPJS = await bridgeKTP(args.no_kartu, time);
            let dataPasien = await getLocalDataByNIK(args.no_kartu);
            var data = `{` +
                `"dataPasien": { ` +
                    `"no_medrec": "${dataPasien[0].no_medrec}", ` +
                    `"sts": "${dataPasien[0].sts}", ` +
                    `"nama": "${dataPasien[0].nama}", ` +
                    `"nama1": "${dataPasien[0].nama1}", ` +
                    `"nama2": "${dataPasien[0].nama2}", ` +
                    `"nama3": "${dataPasien[0].nama3}", ` +
                    `"id": "${dataPasien[0].id}", ` +
                    `"no_ktp": "${dataPasien[0].no_ktp}", ` +
                    `"nama_panggil": "${dataPasien[0].nama_panggil}", ` +
                    `"kelamin": "${dataPasien[0].kelamin}", ` +
                    `"tempat": "${dataPasien[0].tempat}", ` +
                    `"tgl_lahir": "${dataPasien[0].tgl_lahir}", ` +
                    `"tgl": "${dataPasien[0].tgl}", ` +
                    `"bulan": "${dataPasien[0].bulan}", ` +
                    `"tahun": "${dataPasien[0].tahun}", ` +
                    `"status": "${dataPasien[0].status}", ` +
                    `"agama": "${dataPasien[0].agama}", ` +
                    `"pekerjaan": "${dataPasien[0].pekerjaan}", ` +
                    `"perusahaan": "${dataPasien[0].perusahaan}", ` +
                    `"desa": "${dataPasien[0].desa}", ` +
                    `"jln1": "${dataPasien[0].jln1}", ` +
                    `"no": "${dataPasien[0].no}", ` +
                    `"rt": "${dataPasien[0].rt}", ` +
                    `"rw": "${dataPasien[0].rw}", ` +
                    `"kecamatan": "${dataPasien[0].kecamatan}", ` +
                    `"kota": "${dataPasien[0].kota}", ` +
                    `"telp": "${dataPasien[0].telp}", ` +
                    `"hubungan": "${dataPasien[0].hubungan}", ` +
                    `"id2": "${dataPasien[0].id2}", ` +
                    `"no_ktp2": "${dataPasien[0].no_ktp2}", ` +
                    `"sts2": "${dataPasien[0].sts2}", ` +
                    `"nama_penjamin": "${dataPasien[0].nama_penjamin}", ` +
                    `"nama_panggil2": "${dataPasien[0].nama_panggil2}", ` +
                    `"nama21": "${dataPasien[0].nama21}", ` +
                    `"nama22": "${dataPasien[0].nama22}", ` +
                    `"nama23": "${dataPasien[0].nama23}", ` +
                    `"pekerjaan2": "${dataPasien[0].pekerjaan2}", ` +
                    `"perusahaan2": "${dataPasien[0].perusahaan2}", ` +
                    `"jln2": "${dataPasien[0].jln2}", ` +
                    `"desa2": "${dataPasien[0].desa2}", ` +
                    `"no2": "${dataPasien[0].no2}", ` +
                    `"rt2": "${dataPasien[0].rt2}", ` +
                    `"rw2": "${dataPasien[0].rw2}", ` +
                    `"kecamatan2": "${dataPasien[0].kecamatan2}", ` +
                    `"kota2": "${dataPasien[0].kota2}", ` +
                    `"telp2": "${dataPasien[0].telp2}", ` +
                    `"userid": "${dataPasien[0].userid}", ` +
                    `"jam": "${dataPasien[0].jam}", ` +
                    `"xtgl": "${dataPasien[0].xtgl}", ` +
                    `"komplek1": "${dataPasien[0].komplek1}", ` +
                    `"komplek2": "${dataPasien[0].komplek2}", ` +
                    `"tempat2": "${dataPasien[0].tempat2}", ` +
                    `"tgl2": "${dataPasien[0].tgl2}", ` +
                    `"bln2": "${dataPasien[0].bln2}", ` +
                    `"tahun2": "${dataPasien[0].tahun2}", ` +
                    `"flag_cetak": "${dataPasien[0].flag_cetak}", ` +
                    `"jnspt1": "${dataPasien[0].jnspt1}", ` +
                    `"jnspt2": "${dataPasien[0].jnspt2}" ` +
                    `},` +
                `"dataBPJS": { ` +
                    `"nik": "${dataBPJS.response.peserta.nik}", ` +
                    `"noKartu": "${dataBPJS.response.peserta.noKartu}", ` +
                    `"nama": "${dataBPJS.response.peserta.nama}", ` +
                    `"pisa": "${dataBPJS.response.peserta.pisa}", ` +
                    `"sex": "${dataBPJS.response.peserta.sex}", ` +
                    `"cob_nmAsuransi": "${dataBPJS.response.peserta.cob.nmAsuransi}", ` +
                    `"cob_noAsuransi": "${dataBPJS.response.peserta.cob.noAsuransi}", ` +
                    `"cob_tglTAT": "${dataBPJS.response.peserta.cob.tglTAT}", ` +
                    `"cob_tglTMT": "${dataBPJS.response.peserta.cob.tglTMT}", ` +
                    `"hakKelas_keterangan": "${dataBPJS.response.peserta.hakKelas.keterangan}", ` +
                    `"hakKelas_kode": "${dataBPJS.response.peserta.hakKelas.kode}", ` +
                    `"informasi_dinsos": "${dataBPJS.response.peserta.informasi.dinsos}", ` +
                    `"informasi_noSKTM": "${dataBPJS.response.peserta.informasi.noSKTM}", ` +
                    `"informasi_prolanisPRB": "${dataBPJS.response.peserta.informasi.prolanisPRB}", ` +
                    `"jenisPeserta_keterangan": "${dataBPJS.response.peserta.jenisPeserta.keterangan}", ` +
                    `"jenisPeserta_kode": "${dataBPJS.response.peserta.jenisPeserta.kode}", ` +
                    `"mr_noMR": "${dataBPJS.response.peserta.mr.noMR}", ` +
                    `"mr_noTelepon": "${dataBPJS.response.peserta.mr.noTelepon}", ` +
                    `"provUmum_kdProvider": "${dataBPJS.response.peserta.provUmum.kdProvider}", ` +
                    `"provUmum_nmProvider": "${dataBPJS.response.peserta.provUmum.nmProvider}", ` +
                    `"statusPeserta_keterangan": "${dataBPJS.response.peserta.statusPeserta.keterangan}", ` +
                    `"statusPeserta_kode": "${dataBPJS.response.peserta.statusPeserta.kode}", ` +
                    `"tglCetakKartu": "${dataBPJS.response.peserta.tglCetakKartu}", ` +
                    `"tglLahir": "${dataBPJS.response.peserta.tglLahir}", ` +
                    `"tglTAT": "${dataBPJS.response.peserta.tglTAT}", ` +
                    `"tglTMT": "${dataBPJS.response.peserta.tglTMT}", ` +
                    `"umur_umurSaatPelayanan": "${dataBPJS.response.peserta.umur.umurSaatPelayanan}", ` +
                    `"umur_umurSekarang": "${dataBPJS.response.peserta.umur.umurSekarang}" ` +
                    `},` +
                `"status": {` +
                    `"data_bpjs": "${dataBPJS.metaData.message}", ` +
                    `"data_lokal": "${dataPasien[1]}" ` +
                    `}` +
                `}`;
            let result = JSON.parse(data);
            return result;
        },

        async searchByBPJS(_: any, args: any) {
            const time = await moment().format('YYYY-MM-DD');
            let dataBPJS = await bridgeBPJS(args.no_kartu, time);
            let dataPasien = await getLocalDataByNIK(dataBPJS.response.peserta.nik);
            var data = `{` +
                `"dataPasien": { ` +
                    `"no_medrec": "${dataPasien[0].no_medrec}", ` +
                    `"sts": "${dataPasien[0].sts}", ` +
                    `"nama": "${dataPasien[0].nama}", ` +
                    `"nama1": "${dataPasien[0].nama1}", ` +
                    `"nama2": "${dataPasien[0].nama2}", ` +
                    `"nama3": "${dataPasien[0].nama3}", ` +
                    `"id": "${dataPasien[0].id}", ` +
                    `"no_ktp": "${dataPasien[0].no_ktp}", ` +
                    `"nama_panggil": "${dataPasien[0].nama_panggil}", ` +
                    `"kelamin": "${dataPasien[0].kelamin}", ` +
                    `"tempat": "${dataPasien[0].tempat}", ` +
                    `"tgl_lahir": "${dataPasien[0].tgl_lahir}", ` +
                    `"tgl": "${dataPasien[0].tgl}", ` +
                    `"bulan": "${dataPasien[0].bulan}", ` +
                    `"tahun": "${dataPasien[0].tahun}", ` +
                    `"status": "${dataPasien[0].status}", ` +
                    `"agama": "${dataPasien[0].agama}", ` +
                    `"pekerjaan": "${dataPasien[0].pekerjaan}", ` +
                    `"perusahaan": "${dataPasien[0].perusahaan}", ` +
                    `"desa": "${dataPasien[0].desa}", ` +
                    `"jln1": "${dataPasien[0].jln1}", ` +
                    `"no": "${dataPasien[0].no}", ` +
                    `"rt": "${dataPasien[0].rt}", ` +
                    `"rw": "${dataPasien[0].rw}", ` +
                    `"kecamatan": "${dataPasien[0].kecamatan}", ` +
                    `"kota": "${dataPasien[0].kota}", ` +
                    `"telp": "${dataPasien[0].telp}", ` +
                    `"hubungan": "${dataPasien[0].hubungan}", ` +
                    `"id2": "${dataPasien[0].id2}", ` +
                    `"no_ktp2": "${dataPasien[0].no_ktp2}", ` +
                    `"sts2": "${dataPasien[0].sts2}", ` +
                    `"nama_penjamin": "${dataPasien[0].nama_penjamin}", ` +
                    `"nama_panggil2": "${dataPasien[0].nama_panggil2}", ` +
                    `"nama21": "${dataPasien[0].nama21}", ` +
                    `"nama22": "${dataPasien[0].nama22}", ` +
                    `"nama23": "${dataPasien[0].nama23}", ` +
                    `"pekerjaan2": "${dataPasien[0].pekerjaan2}", ` +
                    `"perusahaan2": "${dataPasien[0].perusahaan2}", ` +
                    `"jln2": "${dataPasien[0].jln2}", ` +
                    `"desa2": "${dataPasien[0].desa2}", ` +
                    `"no2": "${dataPasien[0].no2}", ` +
                    `"rt2": "${dataPasien[0].rt2}", ` +
                    `"rw2": "${dataPasien[0].rw2}", ` +
                    `"kecamatan2": "${dataPasien[0].kecamatan2}", ` +
                    `"kota2": "${dataPasien[0].kota2}", ` +
                    `"telp2": "${dataPasien[0].telp2}", ` +
                    `"userid": "${dataPasien[0].userid}", ` +
                    `"jam": "${dataPasien[0].jam}", ` +
                    `"xtgl": "${dataPasien[0].xtgl}", ` +
                    `"komplek1": "${dataPasien[0].komplek1}", ` +
                    `"komplek2": "${dataPasien[0].komplek2}", ` +
                    `"tempat2": "${dataPasien[0].tempat2}", ` +
                    `"tgl2": "${dataPasien[0].tgl2}", ` +
                    `"bln2": "${dataPasien[0].bln2}", ` +
                    `"tahun2": "${dataPasien[0].tahun2}", ` +
                    `"flag_cetak": "${dataPasien[0].flag_cetak}", ` +
                    `"jnspt1": "${dataPasien[0].jnspt1}", ` +
                    `"jnspt2": "${dataPasien[0].jnspt2}" ` +
                    `},` +
                `"dataBPJS": { ` +
                    `"nik": "${dataBPJS.response.peserta.nik}", ` +
                    `"noKartu": "${dataBPJS.response.peserta.noKartu}", ` +
                    `"nama": "${dataBPJS.response.peserta.nama}", ` +
                    `"pisa": "${dataBPJS.response.peserta.pisa}", ` +
                    `"sex": "${dataBPJS.response.peserta.sex}", ` +
                    `"cob_nmAsuransi": "${dataBPJS.response.peserta.cob.nmAsuransi}", ` +
                    `"cob_noAsuransi": "${dataBPJS.response.peserta.cob.noAsuransi}", ` +
                    `"cob_tglTAT": "${dataBPJS.response.peserta.cob.tglTAT}", ` +
                    `"cob_tglTMT": "${dataBPJS.response.peserta.cob.tglTMT}", ` +
                    `"hakKelas_keterangan": "${dataBPJS.response.peserta.hakKelas.keterangan}", ` +
                    `"hakKelas_kode": "${dataBPJS.response.peserta.hakKelas.kode}", ` +
                    `"informasi_dinsos": "${dataBPJS.response.peserta.informasi.dinsos}", ` +
                    `"informasi_noSKTM": "${dataBPJS.response.peserta.informasi.noSKTM}", ` +
                    `"informasi_prolanisPRB": "${dataBPJS.response.peserta.informasi.prolanisPRB}", ` +
                    `"jenisPeserta_keterangan": "${dataBPJS.response.peserta.jenisPeserta.keterangan}", ` +
                    `"jenisPeserta_kode": "${dataBPJS.response.peserta.jenisPeserta.kode}", ` +
                    `"mr_noMR": "${dataBPJS.response.peserta.mr.noMR}", ` +
                    `"mr_noTelepon": "${dataBPJS.response.peserta.mr.noTelepon}", ` +
                    `"provUmum_kdProvider": "${dataBPJS.response.peserta.provUmum.kdProvider}", ` +
                    `"provUmum_nmProvider": "${dataBPJS.response.peserta.provUmum.nmProvider}", ` +
                    `"statusPeserta_keterangan": "${dataBPJS.response.peserta.statusPeserta.keterangan}", ` +
                    `"statusPeserta_kode": "${dataBPJS.response.peserta.statusPeserta.kode}", ` +
                    `"tglCetakKartu": "${dataBPJS.response.peserta.tglCetakKartu}", ` +
                    `"tglLahir": "${dataBPJS.response.peserta.tglLahir}", ` +
                    `"tglTAT": "${dataBPJS.response.peserta.tglTAT}", ` +
                    `"tglTMT": "${dataBPJS.response.peserta.tglTMT}", ` +
                    `"umur_umurSaatPelayanan": "${dataBPJS.response.peserta.umur.umurSaatPelayanan}", ` +
                    `"umur_umurSekarang": "${dataBPJS.response.peserta.umur.umurSekarang}" ` +
                    `},` +
                `"status": {` +
                    `"data_bpjs": "${dataBPJS.metaData.message}", ` +
                    `"data_lokal": "${dataPasien[1]}" ` +
                    `}` +
                `}`;
            let result = JSON.parse(data);
            return result;
        },
    },

    Mutation: {
        async addPasien(_: any, args: any) {
            let lastUser = await DTPasien.findAll({ order: [['no_medrec', 'DESC']], limit: 1 });
            const medrec = parseInt(lastUser[0].no_medrec, 10) + 1;
            return DTPasien.create({
                no_medrec: medrec,
                sts: args.sts,
                nama: args.nama,
                nama1: args.nama1,
                nama2: args.nama2,
                nama3: args.nama3,
                id: args.id,
                no_ktp: args.no_ktp,
                nama_panggil: args.nama_panggil,
                kelamin: args.kelamin,
                tempat: args.tempat,
                tgl_lahir: args.tgl_lahir,
                tgl: args.tgl,
                bulan: args.bulan,
                tahun: args.tahun,
                status: args.status,
                agama: args.agama,
                pekerjaan: args.pekerjaan,
                perusahaan: args.perusahaan,
                desa: args.desa,
                jln1: args.jln1,
                no: args.no,
                rt: args.rt,
                rw: args.rw,
                kecamatan: args.kecamatan,
                kota: args.kota,
                telp: args.telp,
                hubungan: args.hubungan,
                id2: args.id2,
                no_ktp2: args.no_ktp2,
                sts2: args.sts2,
                nama_penjamin: args.nama_penjamin,
                nama_panggil2: args.nama_panggil2,
                nama21: args.nama21,
                nama22: args.nama22,
                nama23: args.nama23,
                pekerjaan2: args.pekerjaan2,
                perusahaan2: args.perusahaan2,
                jln2: args.jln2,
                desa2: args.desa2,
                no2: args.no2,
                rt2: args.rt2,
                rw2: args.rw2,
                kecamatan2: args.kecamatan2,
                kota2: args.kota2,
                telp2: args.telp2,
                userid: args.userid,
                jam: args.jam,
                xtgl: args.xtgl,
                komplek1: args.komplek1,
                komplek2: args.komplek2,
                tempat2: args.tempat2,
                tgl2: args.tgl2,
                bln2: args.bln2,
                tahun2: args.tahun2,
                flag_cetak: args.flag_cetak,
                jnspt1: args.jnspt1,
                jnspt2: args.jnspt2
            })
            .catch(function(err: any) {
                console.log(err);
                throw err;
            });
        },

        async updatePasien(_: any, args: any) {
            await DTPasien.update({
                sts: args.sts,
                nama: args.nama,
                nama1: args.nama1,
                nama2: args.nama2,
                nama3: args.nama3,
                id: args.id,
                no_ktp: args.no_ktp,
                nama_panggil: args.nama_panggil,
                kelamin: args.kelamin,
                tempat: args.tempat,
                tgl_lahir: args.tgl_lahir,
                tgl: args.tgl,
                bulan: args.bulan,
                tahun: args.tahun,
                status: args.status,
                agama: args.agama,
                pekerjaan: args.pekerjaan,
                perusahaan: args.perusahaan,
                desa: args.desa,
                jln1: args.jln1,
                no: args.no,
                rt: args.rt,
                rw: args.rw,
                kecamatan: args.kecamatan,
                kota: args.kota,
                telp: args.telp,
                hubungan: args.hubungan,
                id2: args.id2,
                no_ktp2: args.no_ktp2,
                sts2: args.sts2,
                nama_penjamin: args.nama_penjamin,
                nama_panggil2: args.nama_panggil2,
                nama21: args.nama21,
                nama22: args.nama22,
                nama23: args.nama23,
                pekerjaan2: args.pekerjaan2,
                perusahaan2: args.perusahaan2,
                jln2: args.jln2,
                desa2: args.desa2,
                no2: args.no2,
                rt2: args.rt2,
                rw2: args.rw2,
                kecamatan2: args.kecamatan2,
                kota2: args.kota2,
                telp2: args.telp2,
                userid: args.userid,
                jam: args.jam,
                xtgl: args.xtgl,
                komplek1: args.komplek1,
                komplek2: args.komplek2,
                tempat2: args.tempat2,
                tgl2: args.tgl2,
                bln2: args.bln2,
                tahun2: args.tahun2,
                flag_cetak: args.flag_cetak,
                jnspt1: args.jnspt1,
                jnspt2: args.jnspt2
            }, { where: {no_medrec: args.no_medrec } });
            return DTPasien.findOne({ where: {no_medrec: args.no_medrec} });
        }
    }
};