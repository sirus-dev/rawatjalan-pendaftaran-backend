import Sequelize from 'sequelize';
import { db, functionLogger } from '../lib';

// Model definition for all required table
const UserModel = db.define('users', {
    userid: { type: Sequelize.INTEGER(10), primaryKey: true },
    nodok: { type: Sequelize.STRING(10) },
    nik: { type: Sequelize.STRING(10) },
    email: { type: Sequelize.STRING(100) },
    password: { type: Sequelize.STRING(100) },
    login_date: { type: Sequelize.STRING(30) },
    token: { type: Sequelize.TEXT },
    role: { type: Sequelize.STRING(10) }
}, { freezeTableName: true, timestamps: false });

const DoctorModel = db.define('dtdokter', {
    nodok: { type: Sequelize.STRING, primaryKey: true },
    nama_dok: { type: Sequelize.STRING },
    specialis: { type: Sequelize.STRING },
    pers_kons: { type: Sequelize.DOUBLE },
    pers_tind: { type: Sequelize.DOUBLE },
    tgl_masuk: { type: Sequelize.DATE },
    by_rujuk: { type: Sequelize.DATE, DEFAULT: '0', NULL: false },
    no_urut: { type: Sequelize.STRING },
    nm_panggil: { type: Sequelize.STRING },
    jenkel: { type: Sequelize.STRING },
    tempat: { type: Sequelize.STRING },
    tgl: { type: Sequelize.STRING },
    bln: { type: Sequelize.STRING },
    thn: { type: Sequelize.STRING },
    sts: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    pt: { type: Sequelize.STRING },
    kp: { type: Sequelize.STRING },
    jln: { type: Sequelize.STRING },
    no: { type: Sequelize.STRING },
    rt: { type: Sequelize.STRING },
    rw: { type: Sequelize.STRING },
    desa: { type: Sequelize.STRING },
    kec: { type: Sequelize.STRING },
    kota: { type: Sequelize.STRING },
    bank: { type: Sequelize.STRING },
    an: { type: Sequelize.STRING },
    cabang: { type: Sequelize.STRING },
    no_rek: { type: Sequelize.STRING },
    s1: { type: Sequelize.STRING },
    tahun1: { type: Sequelize.STRING },
    s2: { type: Sequelize.STRING },
    tahun2: { type: Sequelize.STRING },
    s3: { type: Sequelize.STRING },
    tahun3: { type: Sequelize.STRING },
    praktek1: { type: Sequelize.STRING },
    praktek2: { type: Sequelize.STRING },
    praktek3: { type: Sequelize.STRING },
    jam: { type: Sequelize.STRING },
    xtgl: { type: Sequelize.STRING },
    userid: { type: Sequelize.STRING },
    id2: { type: Sequelize.STRING },
    no_ktp: { type: Sequelize.STRING },
    nama_panggil: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    area: { type: Sequelize.STRING },
    telp: { type: Sequelize.STRING },
    areahp1: { type: Sequelize.STRING },
    hp1: { type: Sequelize.STRING },
    areapager: { type: Sequelize.STRING },
    pagerno1: { type: Sequelize.STRING },
    pswtpager: { type: Sequelize.STRING },
    tgls1: { type: Sequelize.STRING },
    blns1: { type: Sequelize.STRING },
    tgls2: { type: Sequelize.STRING },
    blns2: { type: Sequelize.STRING },
    tgls3: { type: Sequelize.STRING },
    blns3: { type: Sequelize.STRING },
    no_sp: { type: Sequelize.STRING },
    no_sip: { type: Sequelize.STRING },
    no_dir: { type: Sequelize.STRING },
    no_sptp: { type: Sequelize.STRING },
    tglsk1: { type: Sequelize.STRING },
    tglsk2: { type: Sequelize.STRING },
    tglsk3: { type: Sequelize.STRING },
    blnsk1: { type: Sequelize.STRING },
    blnsk2: { type: Sequelize.STRING },
    blnsk3: { type: Sequelize.STRING },
    tglsk4: { type: Sequelize.STRING },
    blnsk4: { type: Sequelize.STRING },
    no_npwp: { type: Sequelize.STRING },
    tglnpwp: { type: Sequelize.STRING },
    blnnpwp: { type: Sequelize.STRING },
    thnnpwp: { type: Sequelize.STRING },
    cmbbpk: { type: Sequelize.STRING },
    areahp2: { type: Sequelize.STRING },
    hp2: { type: Sequelize.STRING },
    tahunsk1: { type: Sequelize.STRING },
    tahunsk2: { type: Sequelize.STRING },
    tahunsk3: { type: Sequelize.STRING },
    tahunsk4: { type: Sequelize.STRING },
    garantifee: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    transport: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    email: { type: Sequelize.STRING },
    status: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    tgl_terimanpwp: { type: Sequelize.STRING },
    sts_pegawai: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    ahli: { type: Sequelize.INTEGER, DEFAULT: '1', NULL: false },
    alat_dokter: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    alat_rs: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    kd_poli: { type: Sequelize.STRING },
    sts_pegawai2: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    sts_npwp: { type: Sequelize.INTEGER, DEFAULT: '0', NULL: false },
    npwp1: { type: Sequelize.STRING },
    npwp2: { type: Sequelize.STRING },
    npwp3: { type: Sequelize.STRING },
    npwp4: { type: Sequelize.STRING },
    npwp5: { type: Sequelize.STRING },
    jabatanmedis: { type: Sequelize.STRING },
    tgl_praktek: { type: Sequelize.STRING },
    sts_npwp2: { type: Sequelize.STRING },
    sts_pegawai3: { type: Sequelize.STRING },
    sts_keahlian: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const KaryawanModel = db.define('dtkaryawan', {
    nik: { type: Sequelize.STRING, primaryKey: true },
    nama: { type: Sequelize.STRING },
    panggilan: { type: Sequelize.STRING },
    jnsktp: { type: Sequelize.STRING },
    ktp: { type: Sequelize.STRING },
    jenkel: { type: Sequelize.STRING },
    tempat: { type: Sequelize.STRING },
    tgllhr: { type: Sequelize.STRING },
    blnlhr: { type: Sequelize.STRING },
    thnlhr: { type: Sequelize.STRING },
    sts: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    kp: { type: Sequelize.STRING },
    jln: { type: Sequelize.STRING },
    desa: { type: Sequelize.STRING },
    kecamatan: { type: Sequelize.STRING },
    no: { type: Sequelize.STRING },
    kota: { type: Sequelize.STRING },
    rt: { type: Sequelize.STRING },
    rw: { type: Sequelize.STRING },
    areahp1: { type: Sequelize.STRING },
    hp1: { type: Sequelize.STRING },
    areahp2: { type: Sequelize.STRING },
    hp2: { type: Sequelize.STRING },
    area: { type: Sequelize.STRING },
    telp: { type: Sequelize.STRING },
    areapager: { type: Sequelize.STRING },
    pager1: { type: Sequelize.STRING },
    pager2: { type: Sequelize.STRING },
    photo: { type: Sequelize.STRING },
    bpk: { type: Sequelize.STRING },
    tglmsk: { type: Sequelize.STRING },
    blnmsk: { type: Sequelize.STRING },
    thnmsk: { type: Sequelize.STRING },
    tglklr: { type: Sequelize.STRING },
    thnklr: { type: Sequelize.STRING },
    blnklr: { type: Sequelize.STRING },
    kd_bagian: { type: Sequelize.STRING },
    kd_jabatan: { type: Sequelize.STRING },
    shift: { type: Sequelize.STRING },
    status: { type: Sequelize.STRING },
    kd_unit: { type: Sequelize.STRING },
    no_urut: { type: Sequelize.STRING },
    kd_golongan: { type: Sequelize.STRING },
    kd_ptkp: { type: Sequelize.STRING },
    absensi: { type: Sequelize.INTEGER },
    flag_keluar: { type: Sequelize.STRING },
    nilai: { type: Sequelize.STRING },
    kd_divisi: { type: Sequelize.STRING },
    nm_divisi: { type: Sequelize.STRING },
    flag_paramedis: { type: Sequelize.STRING },
    nm_subdivisi: { type: Sequelize.STRING },
    kd_subdivisi: { type: Sequelize.STRING },
    aktifbekerja: { type: Sequelize.STRING },
    tgl_masuk: { type: Sequelize.STRING },
    sts_kerja: { type: Sequelize.STRING },
    sts_hutang: { type: Sequelize.STRING },
    sts_prg: { type: Sequelize.STRING },
    id_prg: { type: Sequelize.STRING },
    pass_prg: { type: Sequelize.STRING },
    kd_ruang: { type: Sequelize.STRING },
    nm_ruang: { type: Sequelize.STRING },
    level: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const PatientModel = db.define('pasien', {
    id_pasien: { type: Sequelize.INTEGER, primaryKey: true },
    no_medrec: { type: Sequelize.STRING },
    no_ktp: { type: Sequelize.STRING },
    no_kartu: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    tglLahir: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const DTPatientModel = db.define('dtpasien', {
    no_medrec: { type: Sequelize.STRING, primaryKey: true },
    sts: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    nama1: { type: Sequelize.STRING },
    nama2: { type: Sequelize.STRING },
    nama3: { type: Sequelize.STRING },
    id: { type: Sequelize.STRING },
    no_ktp: { type: Sequelize.STRING },
    nama_panggil: { type: Sequelize.STRING },
    kelamin: { type: Sequelize.STRING },
    tempat: { type: Sequelize.STRING },
    tgl_lahir: { type: Sequelize.STRING },
    tgl: { type: Sequelize.STRING },
    bulan: { type: Sequelize.STRING },
    tahun: { type: Sequelize.STRING },
    status: { type: Sequelize.STRING },
    agama: { type: Sequelize.STRING },
    pekerjaan: { type: Sequelize.STRING },
    perusahaan: { type: Sequelize.STRING },
    desa: { type: Sequelize.STRING },
    jln1: { type: Sequelize.STRING },
    no: { type: Sequelize.STRING },
    rt: { type: Sequelize.STRING },
    rw: { type: Sequelize.STRING },
    kecamatan: { type: Sequelize.STRING },
    kota: { type: Sequelize.STRING },
    telp: { type: Sequelize.STRING },
    hubungan: { type: Sequelize.STRING },
    id2: { type: Sequelize.STRING },
    no_ktp2: { type: Sequelize.STRING },
    sts2: { type: Sequelize.STRING },
    nama_penjamin: { type: Sequelize.STRING },
    nama_panggil2: { type: Sequelize.STRING },
    nama21: { type: Sequelize.STRING },
    nama22: { type: Sequelize.STRING },
    nama23: { type: Sequelize.STRING },
    pekerjaan2: { type: Sequelize.STRING },
    perusahaan2: { type: Sequelize.STRING },
    jln2: { type: Sequelize.STRING },
    desa2: { type: Sequelize.STRING },
    no2: { type: Sequelize.STRING },
    rt2: { type: Sequelize.STRING },
    rw2: { type: Sequelize.STRING },
    kecamatan2: { type: Sequelize.STRING },
    kota2: { type: Sequelize.STRING },
    telp2: { type: Sequelize.STRING },
    userid: { type: Sequelize.STRING },
    jam: { type: Sequelize.STRING },
    xtgl: { type: Sequelize.STRING },
    komplek1: { type: Sequelize.STRING },
    komplek2: { type: Sequelize.STRING },
    tempat2: { type: Sequelize.STRING },
    tgl2: { type: Sequelize.STRING },
    bln2: { type: Sequelize.STRING },
    tahun2: { type: Sequelize.STRING },
    flag_cetak: { type: Sequelize.STRING },
    jnspt1: { type: Sequelize.STRING },
    jnspt2: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const PesertaBPJSModel = db.define('bpjs_peserta', {
    id_bpjs_peserta: { type: Sequelize.INTEGER, primaryKey: true },
    nik: { type: Sequelize.STRING },
    noKartu: { type: Sequelize.STRING },
    nama: { type: Sequelize.STRING },
    pisa: { type: Sequelize.STRING },
    sex: { type: Sequelize.STRING },
    cob_nmAsuransi: { type: Sequelize.STRING },
    cob_noAsuransi: { type: Sequelize.STRING },
    cob_tglTAT: { type: Sequelize.DATEONLY },
    cob_tglTMT: { type: Sequelize.DATEONLY },
    hakKelas_keterangan: { type: Sequelize.STRING },
    hakKelas_kode: { type: Sequelize.STRING },
    informasi_dinsos: { type: Sequelize.STRING },
    informasi_noSKTM: { type: Sequelize.STRING },
    informasi_prolanisPRB: { type: Sequelize.STRING },
    jenisPeserta_keterangan: { type: Sequelize.STRING },
    jenisPeserta_kode: { type: Sequelize.STRING },
    mr_noMR: { type: Sequelize.STRING },
    mr_noTelepon: { type: Sequelize.STRING },
    provUmum_kdProvider: { type: Sequelize.STRING },
    provUmum_nmProvider: { type: Sequelize.STRING },
    statusPeserta_keterangan: { type: Sequelize.STRING },
    statusPeserta_kode: { type: Sequelize.STRING },
    tglCetakKartu: { type: Sequelize.DATEONLY },
    tglLahir: { type: Sequelize.DATEONLY },
    tglTAT: { type: Sequelize.DATEONLY },
    tglTMT: { type: Sequelize.DATEONLY },
    umur_umurSaatPelayanan: { type: Sequelize.STRING },
    umur_umurSekarang: { type: Sequelize.STRING },
}, { freezeTableName: true, timestamps: false });

const BPJSPropinsiModel = db.define('bpjs_wilayah_propinsi', {
    id: { type: Sequelize.INTEGER(11), primaryKey: true, allowNull: false },
    kodePropinsi: { type: Sequelize.STRING, allowNull: false },
    namaPropinsi: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    createdBy: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const BPJSKabupatenModel = db.define('bpjs_wilayah_kabupaten', {
    id: { type: Sequelize.INTEGER(11), primaryKey: true, allowNull: false },
    kodePropinsi: { type: Sequelize.STRING, allowNull: false },
    kodeKabupaten: { type: Sequelize.STRING, allowNull: false },
    namaKabupaten: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    createdBy: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const BPJSKecamatanModel = db.define('bpjs_wilayah_kecamatan', {
    id: { type: Sequelize.INTEGER(11), primaryKey: true, allowNull: false },
    kodePropinsi: { type: Sequelize.STRING, allowNull: false },
    kodeKabupaten: { type: Sequelize.STRING },
    kodeKecamatan: { type: Sequelize.STRING, allowNull: false },
    namaKecamatan: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATE },
    createdBy: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const BPSKecamatannModel = db.define('ref_kecamatan', {
    id_kecamatan: { type: Sequelize.INTEGER(11), primaryKey: true, allowNull: false },
    kode_provinsi: { type: Sequelize.STRING(3), allowNull: false },
    kode_kota: { type: Sequelize.STRING(5), allowNull: false },
    kode_kecamatan: { type: Sequelize.STRING(9), allowNull: false },
    nama_kecamatan: { type: Sequelize.STRING }
}, { freezeTableName: true, timestamps: false });

const BPSKelurahanModel = db.define('ref_kelurahan', {
    id_kelurahan: { type: Sequelize.INTEGER(11), primaryKey: true, allowNull: false },
    kode_provinsi: { type: Sequelize.STRING(3), allowNull: false },
    kode_kota: { type: Sequelize.STRING(5), allowNull: false },
    kode_kecamatan: { type: Sequelize.STRING(9), allowNull: false },
    kode_kelurahan: { type: Sequelize.STRING(13), allowNull: false },
    nama_kelurahan: { type: Sequelize.STRING(100) }
}, { freezeTableName: true, timestamps: false });

const PoliModel = db.define('poli', {
    kd_poli: { type: Sequelize.STRING(5), primaryKey: true, allowNull: false },
    nm_poli: { type: Sequelize.STRING(40) }
}, { freezeTableName: true, timestamps: false });

const AntrianModel = db.define('antrian', {
    id_antrian: { type: Sequelize.STRING(10), primaryKey: true, allowNull: false,  },
    kode_unit: { type: Sequelize.STRING(10) },
    poli: { type: Sequelize.STRING(15) },
    nodok: { type: Sequelize.STRING(10) },
    nama_dok: { type: Sequelize.STRING(40) },
    no_registrasi: { type: Sequelize.STRING(10) },
    no_medrec: { type: Sequelize.STRING(6) },
    nama_pasien: { type: Sequelize.STRING(40) },
    no_antrian: { type: Sequelize.INTEGER(3) },
    jaminan: { type: Sequelize.STRING(8) },
    umur: { type: Sequelize.INTEGER(3) },
    tgl_lahir: { type: Sequelize.DATEONLY },
    kelamin: { type: Sequelize.STRING(1) },
    status_pasien: { type: Sequelize.STRING(10) },
    kaji: { type: Sequelize.STRING(12) },
    tgl_antri: { type: Sequelize.DATEONLY },
    flag: { type: Sequelize.INTEGER(1) },
}, { freezeTableName: true, timestamps: true });

const DataPasienPoliModel = db.define('dtpspoli', {
    no_daftar: { type: Sequelize.STRING(22), primaryKey: true, allowNull: false },
    no_urut: { type: Sequelize.STRING(20) },
    no_njm: { type: Sequelize.STRING(30) },
    tgl_kunj: { type: Sequelize.STRING(10) },
    no_medrec: { type: Sequelize.STRING(10) },
    nama: { type: Sequelize.STRING(50) },
    usia: { type: Sequelize.STRING(35) },
    jenkel: { type: Sequelize.STRING(15) },
    kd_dokter2: { type: Sequelize.STRING(10) },
    kd_dokter: { type: Sequelize.STRING(10) },
    nm_dokter: { type: Sequelize.STRING(30) },
    nm_dokter2: { type: Sequelize.STRING(30) },
    cara_bayar: { type: Sequelize.STRING(10) },
    penjamin: { type: Sequelize.STRING(50) },
    ktp_penjamin: { type: Sequelize.STRING(30) },
    panggilan: { type: Sequelize.STRING(35) },
    nm_penjamin1: { type: Sequelize.STRING(50) },
    nm_penjamin2: { type: Sequelize.STRING(30) },
    nm_penjamin3: { type: Sequelize.STRING(30) },
    tempat2: { type: Sequelize.STRING(30) },
    bulan2: { type: Sequelize.STRING(2) },
    tanggal2: { type: Sequelize.STRING(2) },
    tahun2: { type: Sequelize.STRING(4) },
    pekerjaan2: { type: Sequelize.STRING(40) },
    pt2: { type: Sequelize.STRING(10) },
    komplek2: { type: Sequelize.STRING(20) },
    jln2: { type: Sequelize.STRING(30) },
    desa2: { type: Sequelize.STRING(30) },
    kota2: { type: Sequelize.STRING(30) },
    no2: { type: Sequelize.STRING(5) },
    rt2: { type: Sequelize.STRING(5) },
    rw2: { type: Sequelize.STRING(5) },
    telp2: { type: Sequelize.STRING(15) },
    no_daftar2: { type: Sequelize.STRING(30) },
    asal: { type: Sequelize.STRING(1) },
    kd_klasifikasi: { type: Sequelize.STRING(5) },
    spesialis1: { type: Sequelize.STRING(30) },
    prs_penjamin: { type: Sequelize.STRING(40) },
    spesialis2: { type: Sequelize.STRING(30) },
    spesialis3: { type: Sequelize.STRING(30) },
    tgl3: { type: Sequelize.STRING(2) },
    bln3: { type: Sequelize.STRING(2) },
    tahun3: { type: Sequelize.STRING(4) },
    status: { type: Sequelize.STRING(1) },
    jns_ktp: { type: Sequelize.STRING(1) },
    sts_rm: { type: Sequelize.STRING(1) },
    userid: { type: Sequelize.STRING(100) },
    jam: { type: Sequelize.STRING(5) },
    nm_rujuk: { type: Sequelize.STRING(40) },
    kd_perujuk: { type: Sequelize.STRING(10) },
    no_kkpk: { type: Sequelize.STRING(25) },
    type_kartu: { type: Sequelize.STRING(30) },
    flag_cetak: { type: Sequelize.STRING(1) },
    flag_jurnal: { type: Sequelize.STRING(1) },
    sts_kunjungan: { type: Sequelize.STRING(10) },
    kd_prs: { type: Sequelize.STRING(5) },
    nik: { type: Sequelize.STRING(30) },
    hubungan: { type: Sequelize.STRING(1) },
    limit2: { type: Sequelize.INTEGER(10) },
    limitpaket: { type: Sequelize.INTEGER(10) },
    limitbpjs: { type: Sequelize.INTEGER(10) },
    no_sep: { type: Sequelize.STRING(30) },
    nm_klasifikasi: { type: Sequelize.STRING(100) },
    waktu: { type: Sequelize.STRING(10) },
    bridging: { type: Sequelize.STRING(5) },
}, { freezeTableName: true, timestamps: false });

const SEPRequestModel = db.define('bpjs_sep_request', {
    id_bpjs_sep_request: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false },
    noKartu: { type: Sequelize.STRING, allowNull: false },
    tglSep: { type: Sequelize.DATEONLY },
    ppkPelayanan: { type: Sequelize.STRING, allowNull: false },
    jnsPelayanan: { type: Sequelize.STRING, allowNull: false },
    klsRawat: { type: Sequelize.STRING, allowNull: false },
    noMR: { type: Sequelize.STRING, allowNull: false },
    rujukan_asalRujukan: { type: Sequelize.STRING },
    rujukan_tglRujukan: { type: Sequelize.STRING },
    rujukan_noRujukan: { type: Sequelize.STRING },
    rujukan_ppkRujukan: { type: Sequelize.STRING },
    catatan: { type: Sequelize.STRING },
    diagAwal: { type: Sequelize.STRING },
    poli_tujuan: { type: Sequelize.STRING },
    poli_eksekutif: { type: Sequelize.STRING },
    cob_cob: { type: Sequelize.STRING },
    katarak_katarak: { type: Sequelize.STRING },
    jmn_lakaLantas: { type: Sequelize.STRING },
    jmn_pnjmn_penjamin: { type: Sequelize.STRING },
    jmn_pnjmn_tglKejadian: { type: Sequelize.STRING },
    jmn_pnjmn_keterangan: { type: Sequelize.STRING },
    jmn_pnjmn_spls_suplesi: { type: Sequelize.STRING },
    jmn_pnjmn_spls_noSepSuplesi: { type: Sequelize.STRING },
    jmn_pnjmn_spls_lksLaka_kdPropinsi: { type: Sequelize.STRING },
    jmn_pnjmn_spls_lksLaka_kdKabupaten: { type: Sequelize.STRING },
    jmn_pnjmn_spls_lksLaka_kdKecamatan: { type: Sequelize.STRING },
    skdp_noSurat: { type: Sequelize.STRING },
    skdp_kodeDPJP: { type: Sequelize.STRING },
    noTelp: { type: Sequelize.STRING },
    user: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATEONLY },
    createdBy: { type: Sequelize.STRING },
}, { freezeTableName: true, timestamps: false });

const SEPResponseModel = db.define('bpjs_sep_response', {
    id_bpjs_sep_response: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false },
    id_bpjs_sep_request: { type: Sequelize.INTEGER, allowNull: false },
    noSep: { type: Sequelize.STRING, allowNull: false },
    catatan: { type: Sequelize.STRING },
    diagnosa: { type: Sequelize.STRING },
    jnsPelayanan: { type: Sequelize.STRING, allowNull: false },
    kelasRawat: { type: Sequelize.STRING, allowNull: false },
    penjamin: { type: Sequelize.STRING },
    peserta_asuransi: { type: Sequelize.STRING },
    peserta_hakKelas: { type: Sequelize.STRING },
    peserta_jnsPeserta: { type: Sequelize.STRING },
    peserta_kelamin: { type: Sequelize.STRING },
    peserta_nama: { type: Sequelize.STRING },
    peserta_noKartu: { type: Sequelize.STRING },
    peserta_noMr: { type: Sequelize.STRING },
    peserta_tglLahir: { type: Sequelize.STRING },
    informasi_Dinsos: { type: Sequelize.STRING },
    informasi_prolanisPRB: { type: Sequelize.STRING },
    informasi_noSKTM: { type: Sequelize.STRING },
    poli: { type: Sequelize.STRING },
    poliEksekutif: { type: Sequelize.STRING },
    tglSep: { type: Sequelize.STRING },
    createdAt: { type: Sequelize.DATEONLY },
    createdBy: { type: Sequelize.STRING },
}, { freezeTableName: true, timestamps: false });

UserModel.belongsTo(DoctorModel, {foreignKey: 'nodok'});
UserModel.belongsTo(KaryawanModel, {foreignKey: 'nik'});

// Export defined models
const User = db.models.users;
const Dokter = db.models.dtdokter;
const Karyawan = db.models.dtkaryawan;
const Patient = db.models.pasien;
const DTPasien = db.models.dtpasien;
const PesertaBPJS = db.models.bpjs_peserta;
const Propinsi = db.models.bpjs_wilayah_propinsi;
const Kabupaten = db.models.bpjs_wilayah_kabupaten;
const BPJSKecamatan = db.models.bpjs_wilayah_kecamatan;
const BPSKecamatan = db.models.ref_kecamatan;
const Kelurahan = db.models.ref_kelurahan;
const Poli = db.models.poli;
const Antrian = db.models.antrian;
const PasienPoli = db.models.dtpspoli;
const SEPRequest = db.models.bpjs_sep_request;
const SEPResponse = db.models.bpjs_sep_response;
export {
    User,
    Dokter,
    Karyawan,
    Patient,
    DTPasien,
    PesertaBPJS,
    Propinsi,
    Kabupaten,
    BPJSKecamatan,
    BPSKecamatan,
    Kelurahan,
    Poli,
    Antrian,
    PasienPoli,
    SEPRequest,
    SEPResponse
};

/**
 * Function to get last id from intended table
 * @param modelName Model name where id needed
 * @param sortingRow Intended row in selected model
 */
export async function getLastID(modelName: any, sortingRow: any) {
    functionLogger.debug(`Model: ${modelName}, Row: ${sortingRow}`);

    // search all data in selected table and sort it by selected row
    let result = await modelName.findOne({ order: [[sortingRow, 'DESC']], limit: 1, raw: true });

    // if no result/table is empty, return 0 as current id
    if ( result == null ) {
        result = {
            [sortingRow]: '0'
        };
    }

    // return current id
    functionLogger.debug('Current Id: ' + result[sortingRow]);
    return result[sortingRow];
}