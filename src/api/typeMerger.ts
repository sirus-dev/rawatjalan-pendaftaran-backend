import { mergeTypes } from 'merge-graphql-schemas';
import { pasienType } from './identitas/schema';
import { wilayahType } from './wilayah/schema';
import { rujukanType } from './rujukan/schema';
import { antrianType } from './poli/schema';
import { sepType } from './SEP/schema';
import { masterType } from './master_schema';

// Merge all types into 1 variables
const typeDefs = mergeTypes([
    masterType,
    pasienType,
    wilayahType,
    rujukanType,
    antrianType,
    sepType
]);

export { typeDefs };