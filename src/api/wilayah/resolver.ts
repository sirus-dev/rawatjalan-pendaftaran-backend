import { Propinsi, Kabupaten, BPJSKecamatan, BPSKecamatan, Kelurahan } from '../models';

export const wilayahResolver = {

    Query: {
        getPropinsi(_: any, args: any) {
            return Propinsi.findAll(args)
            .catch(function(err: any) {
                console.log(err);
            });
        },
        getKabupaten(_: any, args: any) {
            return Kabupaten.findAll({ where: args });
        },
        getKecamatan(_: any, args: any) {
            return BPJSKecamatan.findAll({ where: args });
        },
        async getKelurahan(_: any, args: any) {
            const nmKecamatan = await BPJSKecamatan.findOne({ where: args, raw: true });
            const kdKecamatan = await BPSKecamatan.findOne({
                where: { nama_kecamatan: nmKecamatan.namaKecamatan},
                raw: true
            });
            return Kelurahan.findAll({ where: { kode_kecamatan: kdKecamatan.kode_kecamatan } });
        }
    },
};