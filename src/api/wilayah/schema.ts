export const wilayahType = `
type Propinsi {
    id: Int
    kodePropinsi: String
    namaPropinsi: String
    createdAt: String
    createdBy: String
}

type Kabupaten {
    id: Int
    kodePropinsi: String
    kodeKabupaten: String
    namaKabupaten: String
    createdAt: String
    createdBy: String
}

type Kecamatan {
    id: Int
    kodePropinsi: String
    kodeKabupaten: String
    kodeKecamatan: String
    namaKecamatan: String
    createdAt: String
    createdBy: String
}

type Kelurahan {
    id_kelurahan: Int
    kode_provinsi: String
    kode_kota: String
    kode_kecamatan: String
    kode_kelurahan: String
    nama_kelurahan: String
}

type Query {
    getPropinsi: [Propinsi]
    getKabupaten(
        kodePropinsi: String
    ): [Kabupaten]
    getKecamatan(
        kodeKabupaten: String
    ): [Kecamatan]
    getKelurahan(
        kodeKecamatan: String
    ): [Kelurahan]
}

schema{
    query: Query
}
`;