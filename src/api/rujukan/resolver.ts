import { bridgeRujukan, functionLogger } from '../../lib';

export const rujukanResolver = {

    Query: {
        async getListRujukan(_: any, args: any) {
            functionLogger.trace(args);
            let data = await bridgeRujukan(args.no_kartu);
            functionLogger.trace(data);
            return data;
        },
    },
};