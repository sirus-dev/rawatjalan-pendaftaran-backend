export const rujukanType = `
type Rujukan {
    diagnosa: DiagnosaRujukan
    keluhan: String
    noKunjungan: String
    pelayanan: PelayananRujukan
    peserta: PesertaRujukan
    poliRujukan: PoliRujukan
    provPerujuk: ProvPerujuk
    tglKunjungan: String
}

type PesertaRujukan {
    cob: COBRujukan
    hakKelas: HakKelasRujukan
    informasi: InformasiRujukan
    jenisPeserta: JenisPesertaRujukan
    mr: MRRujukan
    nama: String
    nik: String
    noKartu: String
    pisa: String
    provUmum: ProvUmum
    sex: String
    statusPeserta: StatusPesertaRujukan
    tglCetakKartu: String
    tglLahir: String
    tglTAT: String
    tglTMT: String
    umur: Umur
}

type DiagnosaRujukan {
    kode: String
    nama: String
}

type PelayananRujukan {
    kode: String
    nama: String
}

type PoliRujukan {
    kode: String
    nama: String
}

type ProvPerujuk {
    kode: String
    nama: String
}

type COBRujukan {
    nmAsuransi: String
    noAsuransi: String
    tglTAT: String
    tglTMT: String
}

type HakKelasRujukan {
    keterangan: String
    kode: String
}

type InformasiRujukan {
    dinsos: String
    noSKTM: String
    prolanisPRB: String
}

type JenisPesertaRujukan {
    keterangan: String
    kode: String
}

type MRRujukan {
    noMR: String
    noTelepon: String
}

type ProvUmum {
    kdProvider: String
    nmProvider: String
}

type StatusPesertaRujukan {
    keterangan: String
    kode: String
}

type Umur {
    umurSaatPelayanan: String
    umurSekarang: String
}


type Query{
    getListRujukan(
        noKartu: String
    ): ResRujukan
}

schema{
    query: Query
}
`;