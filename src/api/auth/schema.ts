// Define graphql schema type for User Operations
export const userType = `
type User {
    userid: Int
    nodok: String
    nik: String
    email: String
    login_date: String
    token: String
    role: String
}

type Dokter{
    nodok: String
    nama_dok: String
    specialis: String
    no_urut: String
    nm_panggilan: String
    jenkel: String
    pekerjaan: String
    praktek1: String
    praktek2: String
    praktek3: String
    jam: String
    email: String
}

type Karyawan{
    nik: String
    nama: String
    panggilan: String
    photo: String
    kd_bagian: String
    kd_jabatan: String
    no_urut: String
    kd_golongan: String
    kd_divisi: String
    nm_divisi: String
    kd_subdivisi: String
    nm_subdivisi: String
}

type Response {
    status: String
    message: String
}

type Query{
    checkUserEmail(
        email: String!
    ): Response
    getLoginToken(
        email: String!
        password: String!
    ): Response
}

type Mutation{
   registerUser(
        nodok: String
        nik: String
        email: String!
        password: String!
        role: String!
   ): User
   updateUserPassword(
        email: String!
        password: String!
   ): User
   deleteUser(
        userid: Int!
   ): Response
}

schema{
    query: Query
    mutation: Mutation
}
`;