import { User, Dokter, Karyawan, getLastID } from '../models';
import * as crypto from 'crypto-js';
import * as moment from 'moment';
import { getToken } from '../../lib/token';

export const userResolver = {

    Query: {
        async checkUserEmail(_: any, args: any) {
            const result = await User.findOne({ where: args });

            // If no user found return "User Not Found" message
            if ( !result  ) {
                let data = JSON.parse('{ "status": "Failed", "message": "User Not Found" }');
                return data;
            } else {
                let data = JSON.parse('{ "status": "Success", "message": "User Found" }');
                return data;
            }
        },
        async getLoginToken(_: any, args: any) {

            // Encrypt password to SHA256 to match the password on User Table
            var password = crypto.SHA256(args.password).toString();

            // Search user with matching email and password, and generatin token payload at the same time
            const payload = await User.findOne({
                attributes: ['userid', 'nodok', 'nik', 'email', 'role'],
                where: {
                    email: args.email,
                    password: password
                },
                include: [{
                    model: Dokter,
                    attributes: ['nodok', 'nama_dok', 'specialis', 'no_urut', 'nm_panggil', 'jenkel',
                    'pekerjaan', 'praktek1', 'praktek2', 'praktek3', 'jam', 'email']
                }, {
                    model: Karyawan,
                    attributes: ['nik', 'nama', 'panggilan', 'photo', 'kd_bagian', 'kd_jabatan',
                    'no_urut', 'kd_golongan', 'kd_divisi', 'nm_divisi', 'kd_subdivisi', 'nm_subdivisi']
                }],
                raw: true
            });

            // if user not found, return wrong password message
            if ( !payload  ) {
                let data = JSON.parse('{ "status": "Failed", "message": "Wrong Password" }');
                return data;
            } else {

                // Set time variable as current time and get token
                var time = moment().format('L k:m:s');
                var token = getToken(payload);

                // Update user login time with current time
                await User.update({
                    login_date: time,
                    token: token
                }, { where: { userid: payload.userid } });

                // Return generated token
                let data = JSON.parse(`{ "status": "Success", "message": "${token}" }`);
                return data;
            }
        }
    },

    Mutation: {
        async registerUser(_: any, args: any) {

            // Get last id on the table
            let id = parseInt(await getLastID(User, 'userid'), 10) + 1;
            await User.create({
                userid: id,
                nodok: args.nodok,
                nik: args.nik,
                email: args.email,
                password: crypto.SHA256(args.password).toString(),
                role: args.role
            });
            var result = await User.findOne({
                where: {
                    userid: id
                },
                include: [{
                    model: Dokter,
                    attributes: ['nodok', 'nama_dok', 'specialis', 'no_urut', 'nm_panggil', 'jenkel',
                    'pekerjaan', 'praktek1', 'praktek2', 'praktek3', 'jam', 'email']
                }, {
                    model: Karyawan,
                    attributes: ['nik', 'nama', 'panggilan', 'photo', 'kd_bagian', 'kd_jabatan',
                    'no_urut', 'kd_golongan', 'kd_divisi', 'nm_divisi', 'kd_subdivisi', 'nm_subdivisi']
                }],
                raw: true
            });
            return result;
        },

        async updateUserPassword(_: any, args: any) {
            await User.update({
                password: crypto.SHA256(args.password).toString()
            }, { where: { email: (args.email) } });
            return await User.findOne({ where: { email: (args.email) } });
        },
        async deleteUser(_: any, args: any) {
            // Find record that matching provided userid
            let user = await User.findOne({ where: { userid: args.userid } });

            // Return failed message if no matching record is found
            if ( user == null ) {
                let data = JSON.parse(`{ "status": "Failed", "message": "User Not Found" }`);
            }

            // Record with matching id will be destroyed
            return User.destroy({ where: { userid: (args.userid) } })

            // return message that record is susccessfully destroyed
            .then(function() {
                let data = JSON.parse(`{ "status": "Success", "message": "userid ${args.userid} Deleted" }`);
                return data;
            })

            // throw if error found
            .catch((err) => {
                throw err;
            });
        },
    },
};