import { mergeResolvers } from 'merge-graphql-schemas';
import { pasienResolver } from './identitas/resolver';
import { wilayahResolver } from './wilayah/resolver';
import { rujukanResolver } from './rujukan/resolver';
import { antrianResolver } from './poli/resolver';
import { sepResolver } from './SEP/resolver';

// Merge all resolver into 1 variable
const resolvers = mergeResolvers([
    pasienResolver,
    wilayahResolver,
    rujukanResolver,
    antrianResolver,
    sepResolver
]);

export { resolvers };