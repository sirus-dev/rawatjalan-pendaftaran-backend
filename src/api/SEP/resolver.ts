import { bridgeSEP } from '../../lib';

export const sepResolver = {

    Mutation: {
        async insertSEP(_: any, args: any) {
            return bridgeSEP(args);
        },
    },
};

/**
        noKartu: String
        tglSep: String
        ppkPelayanan: String
        jnsPelayanan: String
        klsRawat: String
        noMR: String
        rujukan: {
            asalRujukan: String
            tglRujukan: String
            noRujukan: String
            ppkRujukan: String
        }
        catatan: String
        diagAwal: String
        poli: {
            tujuan: String
            eksekutif: String
        }
        cob: {
            cob: String
        }
        katarak: {
            katarak: String
        }
        jaminan: {
            lakaLantas: String
            penjamin: {
                penjamin: String
                tglKejadian: String
                keterangan: String
                suplesi: {
                    suplesi: String
                    noSepSuplesi: String
                    lokasiLaka: {
                        kdPropinsi: String
                        kdKabupaten: String
                        kdKecamatan: String
                    }
                }
            }
        }
        skdp: {
            noSurat: String
            kodeDPJP: String
        }
        noTelp: String
        user: String
 */