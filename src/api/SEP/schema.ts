export const sepType = `
type SEP {
    catatan: String
    diagnosa: String
    jnsPelayanan: String
    kelasRawat: String
    noSep: String
    penjamin: String
    peserta: PesertaSEP
    informasi: InformasiSEP
    poli: String
    poliEksekutif: String
    tglSep: String
}

type PesertaSEP {
    asuransi: String
    hakKelas: String
    jnsPeserta: String
    kelamin: String
    nama: String
    noKartu: String
    noMr: String
    tglLahir: String
}

type InformasiSEP {
    Dinsos: String
    prolanisPRB: String
    noSKTM: String
}

type Mutation {
    insertSEP (
        noKartu: String
        tglSep: String
        ppkPelayanan: String
        jnsPelayanan: String
        klsRawat: String
        noMR: String
        rujukan_asalRujukan: String
        rujukan_tglRujukan: String
        rujukan_noRujukan: String
        rujukan_ppkRujukan: String
        catatan: String
        diagAwal: String
        poli_tujuan: String
        poli_eksekutif: String
        cob: String
        katarak: String
        jaminan_lakaLantas: String
        jaminan_penjamin: String
        jaminan_penjamin_tglKejadian: String
        jaminan_penjamin_keterangan: String
        jaminan_penjamin_suplesi: String
        jaminan_penjamin_suplesi_noSepSuplesi: String
        jaminan_penjamin_suplesi_lokasiLaka_kdPropinsi: String
        jaminan_penjamin_suplesi_lokasiLaka_kdKabupaten: String
        jaminan_penjamin_suplesi_lokasiLaka_kdKecamatan: String
        skdp_noSurat: String
        skdp_kodeDPJP: String
        noTelp: String
        user: String
    ): ResSEP
}

schema{
    mutation: Mutation
}
`;