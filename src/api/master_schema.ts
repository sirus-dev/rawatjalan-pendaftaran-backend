// Define master type for bridging operation query
// All condensed into one type so in the future it would easier to add new response
export const masterType = `
type MetaData {
    code: String
    message: String
}

type ResRujukan {
    metaData: MetaData
    response: ResponseRujukan
}

type ResponseRujukan {
    rujukan: [Rujukan]
}

type ResSEP {
    metaData: MetaData
    response: ResponseSEP
}

type ResponseSEP {
    sep: SEP
}
`;