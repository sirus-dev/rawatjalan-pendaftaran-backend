// Define graphql schema type for User Operations
export const antrianType = `
type Poli {
    kd_poli: String
    nm_poli: String
    dokter: [Dokter]
}

type Antrian {
    id_antrian: String
    kode_unit: String
    poli: String
    nodok: String
    nama_dok: String
    no_registrasi: String
    no_medrec: String
    nama_pasien: String
    no_antrian: Int
    jaminan: String
    umur: Int
    tgl_lahir: String
    kelamin: String
    status_pasien: String
    kaji: String
    tgl_antri: String
    flag: Int
}

type Dokter{
    nodok: String
    nama_dok: String
    specialis: String
    no_urut: String
    nm_panggilan: String
    jenkel: String
    pekerjaan: String
    praktek1: String
    praktek2: String
    praktek3: String
    jam: String
    email: String
}

type PasienPoli {
    no_daftar: String
    no_urut: String
    no_njm: String
    tgl_kunj: String
    no_medrec: String
    nama: String
    usia: String
    jenkel: String
    kd_dokter2: String
    kd_dokter: String
    nm_dokter: String
    nm_dokter2: String
    cara_bayar: String
    penjamin: String
    ktp_penjamin: String
    panggilan: String
    nm_penjamin1: String
    nm_penjamin2: String
    nm_penjamin3: String
    tempat2: String
    bulan2: String
    tanggal2: String
    tahun2: String
    pekerjaan2: String
    pt2: String
    komplek2: String
    jln2: String
    desa2: String
    kota2: String
    no2: String
    rt2: String
    rw2: String
    telp2: String
    no_daftar2: String
    asal: String
    kd_klasifikasi: String
    spesialis1: String
    prs_penjamin: String
    spesialis2: String
    spesialis3: String
    tgl3: String
    bln3: String
    tahun3: String
    status: String
    jns_ktp: String
    sts_rm: String
    userid: String
    jam: String
    nm_rujuk: String
    kd_perujuk: String
    no_kkpk: String
    type_kartu: String
    flag_cetak: String
    flag_jurnal: String
    sts_kunjungan: String
    kd_prs: String
    nik: String
    hubungan: String
    limit2: Int
    limitpaket: Int
    limitbpjs: Int
    no_sep: String
    nm_klasifikasi: String
    waktu: String
    bridging: String
}

type Query {
    listPoli: [Poli]
    listDokterByKdPoli(
        kd_poli: String
    ): Poli
    listPasienTerdaftar: [PasienPoli]
    getSEPPasien(
        no_daftar: String
    ): SEP
}

type Mutation {
    createAntrian(
        no_medrec: String!
        kd_poli: String!
        nodok: String!
        tgl: String!
    ): Antrian
    savePendaftaranPoli(
        no_urut: String
        no_njm: String
        tgl_kunj: String
        no_medrec: String
        nama: String
        usia: String
        jenkel: String
        kd_dokter2: String
        kd_dokter: String
        nm_dokter: String
        nm_dokter2: String
        cara_bayar: String
        penjamin: String
        ktp_penjamin: String
        panggilan: String
        nm_penjamin1: String
        nm_penjamin2: String
        nm_penjamin3: String
        tempat2: String
        bulan2: String
        tanggal2: String
        tahun2: String
        pekerjaan2: String
        pt2: String
        komplek2: String
        jln2: String
        desa2: String
        kota2: String
        no2: String
        rt2: String
        rw2: String
        telp2: String
        no_daftar2: String
        asal: String
        kd_klasifikasi: String
        spesialis1: String
        prs_penjamin: String
        spesialis2: String
        spesialis3: String
        tgl3: String
        bln3: String
        tahun3: String
        status: String
        jns_ktp: String
        sts_rm: String
        userid: String
        jam: String
        nm_rujuk: String
        kd_perujuk: String
        no_kkpk: String
        type_kartu: String
        flag_cetak: String
        flag_jurnal: String
        sts_kunjungan: String
        kd_prs: String
        nik: String
        hubungan: String
        limit2: Int
        limitpaket: Int
        limitbpjs: Int
        no_sep: String
        nm_klasifikasi: String
        waktu: String
        bridging: String
    ): PasienPoli
    updateStatusPendaftaran(
        no_daftar: String
        sts_kunjungan: String
    ): PasienPoli
}

schema{
    query: Query
    mutation: Mutation
}
`;
