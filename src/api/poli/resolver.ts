import { Poli, Dokter, Antrian, DTPasien, PasienPoli, SEPResponse, getLastID } from '../models';
import * as moment from 'moment';
import { functionLogger, nullifiedSEPData } from '../../lib';
export const antrianResolver = {

    Query: {
        listPoli(_: any, args: any) {
            return Poli.findAll({ where: args });
        },

        async listDokterByKdPoli(_: any, args: any) {

            // search result for poli information
            const poli = await Poli.findOne({ where: args });

            // search result for dokter information
            const doctor = await Dokter.findAll({
                where: { specialis: poli.nm_poli },
                attributes: ['nodok', 'nama_dok', 'specialis', 'no_urut', 'nm_panggil', 'jenkel',
                'pekerjaan', 'praktek1', 'praktek2', 'praktek3', 'jam', 'email'],
                raw: true
            });

            // combine both search result into one variable
            const result = {
                kd_poli: poli.kd_poli,
                nm_poli: poli.nm_poli,
                dokter: doctor
            };

            // return the result
            return result;
        },

        listPasienTerdaftar(_: any, args: any) {
            return PasienPoli.findAll({ where: args });
        },

        async getSEPPasien(_: any, args: any) {

            // search pasien Poli data
            const psPoli = await PasienPoli.findOne({ where: { no_daftar: args.no_daftar } });

            // return nullified data if no pasien poli found
            if ( psPoli == null ) {
                return nullifiedSEPData;
            }

            // search SEP detail where noSep match with Pasien Poli's no_sep
            let data = await SEPResponse.findOne({ where: { noSep: psPoli.no_sep } });

            // map founded SEP data
            const result = {
                catatan: data.catatan,
                diagnosa: data.diagnosa,
                jnsPelayanan: data.jnsPelayanan,
                kelasRawat: data.kelasRawat,
                noSep: data.noSep,
                penjamin: data.penjamin,
                peserta: {
                   asuransi: data.peserta_asuransi,
                   hakKelas: data.peserta_hakKelas,
                   jnsPeserta: data.peserta_jnsPeserta,
                   kelamin: data.peserta_kelamin,
                   nama: data.peserta_nama,
                   noKartu: data.peserta_noKartu,
                   noMr: data.peserta_noMr,
                   tglLahir: data.peserta_tglLahir,
                },
                informasi: {
                   Dinsos: data.informasi_dinsos,
                   prolanisPRB: data.informasi_prolanisPRB,
                   noSKTM: data.informasi_noSKTM,
                },
                poli: data.poli,
                poliEksekutif: data.poliEksekutif,
                tglSep: data.tglSep
            };

            // return mapped data
            functionLogger.trace(result);
            return result;
        },
    },

    Mutation: {
        async createAntrian(_: any, args: any) {

            // Get last ID from Antrian.id_antrian
            let lastAntrian = parseInt(await getLastID(Antrian, 'id_antrian'), 10);

            // Get last ID from Antrian.no_antrian with matching tgl_antri
            let dateAntrian = await Antrian.findOne({ where: { tgl_antri: args.tgl }, order: [['no_antrian', 'DESC']], limit: 1 });

            // set no_antrian to 0 if no previous record of antrian in that date found
            if ( dateAntrian == null ) {
                dateAntrian = {
                    no_antrian: '0'
                };
            }

            // search data from other model
            const poli = await Poli.findOne({ where: { kd_poli: args.kd_poli } });
            const dokter = await Dokter.findOne({ where: { nodok: args.nodok } });
            const pasien = await DTPasien.findOne({ where: { no_medrec: args.no_medrec } });
            let Umur = await getUmur(pasien.tahun, pasien.bulan);
            let tglLahir = `${pasien.tahun}-${pasien.bulan}-${pasien.tgl}`;

            /**
             * Calculate patient age
             * @param thn Patient Birth Year
             * @param bln Patient Birth Month
             */
            function getUmur(thn: any, bln: any) {
                const year = moment().format('YYYY');
                if ( parseInt(bln, 10) < parseInt(moment().format('MM'), 10) ) {
                    const result = (parseInt(year, 10) - thn) + 1;
                    return result;
                } else {
                    const result = parseInt(year, 10) - thn;
                    return result;
                }
            }

            // insert record into database and return the result
            return Antrian.create({
                id_antrian: lastAntrian + 1,
                kode_unit: null,
                poli: poli.nm_poli,
                nodok: dokter.nodok,
                nama_dok: dokter.nama_dok,
                no_registrasi: null,
                no_medrec: pasien.no_medrec,
                nama_pasien: pasien.nama,
                no_antrian: parseInt(dateAntrian.no_antrian, 10) + 1,
                jaminan: null,
                umur: Umur,
                tgl_lahir: tglLahir,
                kelamin: pasien.kelamin,
                status_pasien: 'ada',
                kaji: 'Belum Dikaji',
                tgl_antri: args.tgl,
                flag: 0,
            })

            // catch and throw if error is found
            .catch((err) => {
                functionLogger.error(err);
                throw (err);
            });
        },

        async savePendaftaranPoli(_: any, args: any) {
            let lastPasienID = parseInt(await getLastID(PasienPoli, 'no_daftar'), 10);
            return PasienPoli.create({
                no_daftar: lastPasienID + 1,
                no_urut: args.no_urut,
                no_njm: args.no_njm,
                tgl_kunj: args.tgl_kunj,
                no_medrec: args.no_medrec,
                nama: args.nama,
                usia: args.usia,
                jenkel: args.jenkel,
                kd_dokter2: args.kd_dokter2,
                kd_dokter: args.kd_dokter,
                nm_dokter: args.nm_dokter,
                nm_dokter2: args.nm_dokter2,
                cara_bayar: args.cara_bayar,
                penjamin: args.penjamin,
                ktp_penjamin: args.ktp_penjamin,
                panggilan: args.panggilan,
                nm_penjamin1: args.nm_penjamin1,
                nm_penjamin2: args.nm_penjamin2,
                nm_penjamin3: args.nm_penjamin3,
                tempat2: args.tempat2,
                bulan2: args.bulan2,
                tanggal2: args.tanggal2,
                tahun2: args.tahun2,
                pekerjaan2: args.pekerjaan2,
                pt2: args.pt2,
                komplek2: args.komplek2,
                jln2: args.jln2,
                desa2: args.desa2,
                kota2: args.kota2,
                no2: args.no2,
                rt2: args.rt2,
                rw2: args.rw2,
                telp2: args.telp2,
                no_daftar2: args.no_daftar2,
                asal: args.asal,
                kd_klasifikasi: args.kd_klasifikasi,
                spesialis1: args.spesialis1,
                prs_penjamin: args.prs_penjamin,
                spesialis2: args.spesialis2,
                spesialis3: args.spesialis3,
                tgl3: args.tgl3,
                bln3: args.bln3,
                tahun3: args.tahun3,
                status: args.status,
                jns_ktp: args.jns_ktp,
                sts_rm: args.sts_rm,
                userid: args.userid,
                jam: args.jam,
                nm_rujuk: args.nm_rujuk,
                kd_perujuk: args.kd_perujuk,
                no_kkpk: args.no_kkpk,
                type_kartu: args.type_kartu,
                flag_cetak: args.flag_cetak,
                flag_jurnal: args.flag_jurnal,
                sts_kunjungan: args.sts_kunjungan,
                kd_prs: args.kd_prs,
                nik: args.nik,
                hubungan: args.hubungan,
                limit2: args.limit2,
                limitpaket: args.limitpaket,
                limitbpjs: args.limitbpjs,
                no_sep: args.no_sep,
                nm_klasifikasi: args.nm_klasifikasi,
                waktu: args.waktu,
                bridging: args.bridging,
            })
            .catch((err) => {
                functionLogger.error(err);
                throw err;
            });
        },

        async updateStatusPendaftaran(_: any, args: any) {
            await PasienPoli.update(
                { sts_kunjungan: args.sts_kunjungan },
                { where: { no_daftar: args.no_daftar } }
                );
            return PasienPoli.findOne({ where: { no_daftar: args.no_daftar } });
        }
    },
};