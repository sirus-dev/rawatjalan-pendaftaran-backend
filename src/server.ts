import * as express from 'express';
import * as bodyParser from 'body-parser';
import { createServer } from 'http';
import { graphql, execute, subscribe } from 'graphql';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import { makeExecutableSchema } from 'graphql-tools';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import * as cors from 'cors';
import * as jwt from 'jsonwebtoken';
import * as ejs from 'ejs';
import * as joinMonster from 'join-monster';
import * as joinMonsterAdapt from 'join-monster-graphql-tools-adapter';
import { typeDefs } from './api/typeMerger';
import { resolvers } from './api/resolverMerger';
import { db } from './lib';
import config from './config';
import { userType } from './api/auth/schema';
import { userResolver } from './api/auth/resolver';
const { APP_PORT, LOCALHOST, SECRET } = config;
var router = express.Router();

// Creating Express and defining Public Subscription
const graphQLServer = express();
const pubSub = new PubSub();

// Create Schema from existing Types and Resolvers
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
});

const schemaAuth = makeExecutableSchema({
    typeDefs: userType,
    resolvers: userResolver
});

/**
 * Define server to use cors
 * Set server endpoint "authGraphiql" to generate token for further access
 * Set server endpoint "graphiql" that required proper token to be accessed
 */
graphQLServer.use(cors());
graphQLServer.use('/authGraphql', bodyParser.json(), graphqlExpress({ context: { pubSub, }, schema: schemaAuth }));
graphQLServer.use('/authGraphiql', graphiqlExpress({
    endpointURL: '/authGraphql',
    subscriptionsEndpoint: `ws://${LOCALHOST}:${APP_PORT}/subscriptions`
}));
graphQLServer.use('/graphql', bodyParser.json(), verify, graphqlExpress({ context: { pubSub, }, schema: schema }));
graphQLServer.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql', subscriptionsEndpoint: `ws://${LOCALHOST}:${APP_PORT}/subscriptions`}));

graphQLServer.use('/', function (req: any, res: any) {
    res.json({ message: 'Hello, Welcome to SIRUS Rawat Jalan Pendaftaran API' });
});
/**
 * Middleware function to verify provided token on request header
 */
function verify(req: any, res: any, next: any) {

    return Promise.resolve()
    .then(() => req.get('Authorization'))
    .then((token) => {

        // function return no access message if no token is provided
        if (!token) {
            throw 'You have no access.';
        }
        return token.split(' ');
    })

    // verify the provided token
    .then(([type, payload]) => jwt.verify(payload, SECRET))

    // match the mail inside decoded token payload with email from user table
    .then((decoded) => db.query(`SELECT * FROM users WHERE email="${decoded.email}"`))
    .then((user) => {

        // function throw "Invalid Auth Token" if email from provided token doesn't match any user email
        if (!user) {
            throw 'Invalid auth token provided.';
        }
        return next();
    })

    // throw "No Access" message if function catching an error
    .catch((err) => {
        console.log(err);
        return res.status(400)
        .json({message: err || 'You have no access.'});
    });
}

// Creating Server
const APIServer = createServer(graphQLServer);

// Creating server listener
APIServer.listen(APP_PORT, () => {
    console.log(`App is running on http://${LOCALHOST}:${APP_PORT}/graphiql`);

    // Creating GraphQL Subscription Server using WebSocket
    /*
    const subscriptionServer = SubscriptionServer.create(
        {
            schema,
            execute,
            subscribe,
        },
        {
            server: APIServer,
            path: '/subscriptions',
        },
    );
    */
});