# Changelog

### Wednesday - 28/3/2018 14:00 | Sirus+ IGD API Service 0.1.0-Alpha

* Queue
    - New Model "queue" added
    - New Queries and Mutations added for the model

### Wednesday - 28/3/2018 18:15 | Sirus+ IGD API Service 0.1.1-Alpha

* Assessment
    - New Model "assessment" added
    - New Queries and Mutations added for the model

### Wednesday - 28/3/2018 22:45 | Sirus+ IGD API Service 0.1.2-Alpha

* Triage
    - New Model "triage" added
    - New Queries and Mutations added for the model

### Thursday - 29/3/2018 00:00 | Sirus+ IGD API Service 0.1.3-Alpha

* CT
    - New Model "ct" added
    - New Queries and Mutations added for the model
* DPJP
    - New Model "dpjp" added
    - New Queries and Mutations added for the model
* Perioperatif
    - New Model "perioperatif" added
    - New Queries and Mutations added for the model
* Transfer
    - New Model "transfer" added
    - New Queries and Mutations added for the model

### Monday - 2/4/2018 9:15 | Sirus+ IGD API Service 0.1.4-Alpha

* Edukasi
    - New Model "edukasi" added
    - New Queries and Mutations added for the model

### Monday - 2/4/2018 14:30 | Sirus+ IGD API Service 0.1.5-Alpha

* Bugfix on undetected queries and mutation from 0.1.3-Alpha commmit

### Tuesday - 3/4/2018 16:15 | Sirus+ IGD API Service 0.1.6-Alpha

* Doctor
    - New Model "dtdokter" added
    - New Queries and Mutations added for the model
* Employee
    - New Model "dtkaryawan" added
    - New Queries and Mutations added for the model
* Patient
    - New Model "dtpasien" added
    - New Queries and Mutations added for the model

### Tuesday - 3/4/2018 18:15 | Sirus+ IGD API Service 0.1.7-Alpha

* Edukasi  
    - Bugfix on "addEducation" mutation
    - Few bugfix on update mutation
* Triage
    - Bugfix on few mutation

### Wednesday - 4/4/2018 | Sirus+ IGD API Service 0.1.8-Alpha

* Few updates on existing models

### Wednesday - 4/4/2018 | Sirus+ IGD API Service 0.1.9-Alpha

* Few updates on existing model
* Added new query and mutation for Queue

### Thursday - 5/4/2018 | Sirus+ IGD API Service 0.1.10-Alpha

* Update on dpjp Query and Mutation

### Thursday - 5/4/2018 | Sirus+ IGD API Service 0.1.11-Alpha

* Fix on Queue query "queueByFlag2"

### Friday - 6/4/2018 | Sirus+ IGD API Service 0.1.12-Alpha

* Fix on Queue query "queueByFlag2"

### Monday - 9/4/2018 | Sirus+ IGD API Service 0.1.13-Alpha

* Fix on Queue query "queueByFlag2", now using QueueJoin
* Data structure fix for antrian model

### Monday - 9/4/2018 | Sirus+ IGD API Service 0.1.14-Alpha

* Added few new mutations on Queue model

### Monday 23/4/2018 | Sirus+ IGD API Service 0.1.15-Alpha

* Fix on CT Model data structure

### Tuesday 24/4/2018 | Sirus+ IGD API Service 0.1.16-Alpha

* Assessment
    - Update on assessmen data structure
    - Added new query "assessmentByMedrecLast"
* Triage
    - Added new query "triageByMedrecLast"
* CT
    - Added new query "ctByMedrecLast"

### Tuesday 24/4/2018 | Sirus+ IGD API Service 0.1.16-Hotfix1

* Fix on connector
* Fix on assessment resolver

### Thursday 3/5/2018 | Sirus+ IGD API Service 0.2.0-Beta

* Assessment
    - Added new query "updateAssessmentDokById"
* New model in connector (WIP)

### Saturday 5/5/2018 | Sirus+ IGD API Service 0.2.1-Beta

* Queue
    - Fix on queue resolver for query "queueAllToday"
* Rekonsiliasi Obat
    - Added new model "rekonsiliasi_obat
    - Added new query and mutation for Rekonsiliasi Model
* Rujukan
    - Added new model "rujukan"
    - Added new query and mutation for Rujukan Model
* Pra Anastesi
    - Added new model "pra_anastesi"
    - Added new query and mutation for Pra Anastesi
* Inform Consent
    - Added new model "inform_consent"
    - Added new query and mutation for Inform Consent
* Assessment Dokter
    - Added new model "assessment_dokter"
    - Added new query and mutation for Assessment Model
* Type Inform
    - Added new model "type_inform"
    - Added new query and mutation for Type Inform
* ICD9
    - Added new model "icd9"
    - Added new query and mutation for ICD9
* ICD10
    - Added new model "icd10"
    - Added new query and mutation for ICD10

### Wednesday 9/5/2018 | Sirus+ IGD API Service 0.2.2-Beta

* Death
    - Added new model "death"
    - Added new query and mutation for Death

### Monday 14/5/2018 | Sirus+ IGD API Service 0.2.3-Beta

* Few fixes on Death model and Rujukan model

### Monday 14/5/2018 | Sirus+ IGD API Service 0.2.4-Beta

* Sick Letter
    - Added new model "sickLetter"
    - Added new query and mutation for Sick Letter
* Few mutation fix on existing model